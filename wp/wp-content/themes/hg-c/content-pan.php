<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<div class="pankuzu">
	<ul class="cf">
		<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">TOP</a></li>
		<?php if ( is_home() or is_category() or is_singular('post') or is_year()) { 
		$cat_id=get_query_var('cat');
		$cat=get_category($cat_id);
		$echo_cat = '<li><span><img src="'.get_template_directory_uri().'/common/images/common/img_rsaquo.png" alt=">"></span>'.$cat->cat_name.'</li>';
		$echo_year = sprintf( __( '%s' ), get_the_date( _x( 'Y', 'yearly archives date format' ) ) );
		$echo_year = '<li><span><img src="'.get_template_directory_uri().'/common/images/common/img_rsaquo.png" alt=">"></span>'.$echo_year.'</li>';
		$echo_single = '<li><span><img src="'.get_template_directory_uri().'/common/images/common/img_rsaquo.png" alt=">"></span>'.get_the_title().'</li>';
		?>
		<li><span><img src="<?php echo get_template_directory_uri(); ?>/common/images/common/img_rsaquo.png" alt=">"></span><a href="<?php echo esc_url( home_url() ); ?>/news/">お知らせ</a></li>
		<?php
		if (is_category()){ echo $echo_cat;}
		if (is_year()){ echo $echo_year;}
		if (is_single()){ echo $echo_single;}
		}else{
		
		$postype = get_post_type();
		if (is_page()){
			if (is_parent_slug() === 'contact'){
			echo '<li><span><img src="'.get_template_directory_uri().'/common/images/common/img_rsaquo.png" alt=">"></span><a href="'.get_home_url().'/contact/">'.get_the_title($post->post_parent).'</a></li>';
			}
			echo '<li><span><img src="'.get_template_directory_uri().'/common/images/common/img_rsaquo.png" alt=">"></span>'.get_the_title().'</li>';
		}
		if (is_archive()){
			echo '<li><span><img src="'.get_template_directory_uri().'/common/images/common/img_rsaquo.png" alt=">"></span>';
			if (is_tax()){
				echo '<a href="'.get_post_type_archive_link( $postype ).'">';
			}
			echo get_post_type_object(get_post_type())->label;
			if (is_tax()){
				echo '</a>';
			}
			echo '</li>';
			if (is_tax()){
				$taxonomy = $wp_query->get_queried_object();
				echo '<li><span><img src="'.get_template_directory_uri().'/common/images/common/img_rsaquo.png" alt=">"></span>'.$taxonomy->name.'</li>';
			}
		}
		if (is_single()){
			if (!is_singular('quality')){
				if (is_single('company-top')){
				} else {
					if (is_parent_slug() === 'company-top'){
					echo '<li><span><img src="'.get_template_directory_uri().'/common/images/common/img_rsaquo.png" alt=">"></span><a href="'.get_home_url().'/company/company-top/">'.get_post_type_object(get_post_type())->label.'</a></li>';
					} elseif(is_singular('recruit')) {
						if (is_parent_slug() === 'recruit-top'){
						echo '<li><span><img src="'.get_template_directory_uri().'/common/images/common/img_rsaquo.png" alt=">"></span><a href="'.get_home_url().'/recruit/recruit-top/">'.get_post_type_object(get_post_type())->label.'</a></li>';
						}
					} else {
					echo '<li><span><img src="'.get_template_directory_uri().'/common/images/common/img_rsaquo.png" alt=">"></span><a href="'.get_post_type_archive_link( $postype ).'">'.get_post_type_object(get_post_type())->label.'</a></li>';
					}
				}
			}
			echo '<li><span><img src="'.get_template_directory_uri().'/common/images/common/img_rsaquo.png" alt=">"></span>'.get_the_title().'</li>';
		};
		} end;
		 ?>
	</ul>
</div>