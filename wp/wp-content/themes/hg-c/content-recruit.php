<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php
		if (!is_single('welfare-facilities')){
			if (is_single('small-hydroelectric-generation')){
				the_title( '<h2>', '</h2>' );
			}  else if(!is_single('recruit-top')) {
				the_title( '<h1>', '</h1>' );
			}
		}
		?>
	</header><!-- .entry-header -->

<div class="entry-content">
		<?php
			//スラッグ
			$post_slug = attribute_escape( $post->post_name );

			/* =========
			 採用情報トップ
			===== */
			if($post_slug == 'recruit-top'){

				echo '<div class="messa_top"><div class="messa cf">';
				//写真
				if( get_field('set_photo') ){
					echo '<div class="img_message sp_none">';
					$img = get_field('set_photo');
					$imgurl = wp_get_attachment_image_src($img, 'full');
					$img_message = '<img src="'.$imgurl[0].'" width="100%" alt="">';
					//写真キャプション
					$img_message .= '<div class="re_position">';
					if( get_field('set_photocaption') ){
						$img_message .= nl2br(get_field('set_photocaption'));
					}
					$img_message .= '</div>';
					//写真キャプション
					$img_message .= '<div class="re_name">';
					if( get_field('set_photocaption2') ){
						$img_message .= nl2br(get_field('set_photocaption2'));
					}
					$img_message .= '</div>';
					echo $img_message;
					echo '</div>';
				}
				echo '<div class="messageArea"><h1 class="page-title">メッセージ</h1>';
				//本文
				if( get_field('set_content') ){
					echo '<div>'.wp_kses_post(get_field('set_content')).'</div>';
				}
				echo '</div>';
				//写真
				if( get_field('set_photo') ){
					echo '<div class="img_message pc_none">';
					echo $img_message;
					echo '</div>';
				}

				echo '</div></div>';

				echo '
				<div class="bnrMessa">
					<div class="bnrMessa_inner">
					<div class="bnrMessa_innerBox">
					<a href="'.home_url( '/' ).'senior/">
						<h4>先輩が贈るひとこと</h4>
						<p><img src="'.get_template_directory_uri().'/common/images/common/arrow_gray2.png" width="100%" alt="">詳しく見る</p>
					</a>
					</div>
					</div>
				</div>
				';
				//お知らせ
				get_template_part('news');

				echo '
					<h2 class="ttl_app">募集要項</h2>
					<ul class="appArea cf">';
					$posts = get_posts('include=568&post_type=recruit'); global $post;
					if($posts): foreach($posts as $post): setup_postdata($post);
						echo '
						<li class="shinsotsu"><a href="'.get_the_permalink().'">
							<div class="hover_on"></div>
							<h4>'.get_the_title().'</h4>
							<p>'.esc_html(get_field('lead_sentence')).'</p>
							<p class="btnDeta"><img src="'.get_template_directory_uri().'/common/images/common/arrow_white.png" width="100%" alt="">詳細をみる</p>
						</a></li>
					';
					endforeach; endif; wp_reset_postdata();
					$posts = get_posts('include=570&post_type=recruit'); global $post;
					if($posts): foreach($posts as $post): setup_postdata($post);
						echo '
						<li class="career"><a href="'.get_the_permalink().'">
							<div class="hover_on"></div>
							<h4>'.get_the_title().'</h4>
							<p>'.esc_html(get_field('lead_sentence')).'</p>
							<p class="btnDeta"><img src="'.get_template_directory_uri().'/common/images/common/arrow_white.png" width="100%" alt="">詳細をみる</p>
						</a></li>
					';
					endforeach; endif; wp_reset_postdata();

					echo '</ul>';


				//子ページのリンク
				// $child_posts = get_posts( 'numberposts=-1&order=ASC&orderby=menu_order&post_type=recruit&post_parent=' . $post->ID );
				// if ( $child_posts ) {
				// 	foreach ( $child_posts as $c ) {
				// 		$str = '<div><h3><a href="';
				// 		$str .= $c->guid;
				// 		$str .= '">';
				// 		$str .= $c->post_title;
				// 		$str .= '</a></h3></div>';
				// 		echo $str;
				// 	} //end foreach
				// } //end if
				// echo '<div><h3><a href="'.get_post_type_archive_link('senior').'">'.get_post_type_object('senior')->label.'</a></h3></div>';
			} else {
				/* =========
				 柔軟コンテンツ
				===== */
				get_template_part('flexible');
			}

		?>

</div>
</div>
<div class="centerCont">
	<footer class="entry-footer">
		<?php twentyfifteen_entry_meta(); ?>
		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</div>

</article><!-- #post-## -->
