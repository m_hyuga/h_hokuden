<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php
			if ( is_single() ) :
				the_title( '<h1 class="page-title">', '</h1>' );
			else :
				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
			endif;
		?>
	</header><!-- .entry-header -->
	
	
	<?php
		echo '<div class="entry-content">';
		//業務分類
		$classification = get_the_terms( $post -> post_id, 'classification' );
		if($classification){
			$count = 0;
			foreach($classification as $c){
				echo '<span class="classification">'.esc_html($classification[$count] -> name).'</span>';
				$count++;
			}
		}
		//年度
		$fiscal_year = get_the_terms( $post -> post_id, 'fiscal_year' );
		if($fiscal_year){
			$count = 0;
			foreach($fiscal_year as $f){
				echo '<span class="fiscal_year">'.esc_html($fiscal_year[$count] -> name).'</span>';
				$count++;
			}
		}
		//発注者名
		$client = get_the_terms( $post -> post_id, 'client' );
		if($client){
			echo '<span class="client">'.esc_html($client[0] -> name).'</span>';
		}
		echo '</div>';
		
		// Post thumbnail.
		
			if(has_post_thumbnail()){
				echo '<div class="main_thumb">';
				the_post_thumbnail('full');
				echo '</div>';
			}
	?>
	

	<div class="entry-content">
		<?php 
			//概要
			if($post->post_content!=""){
				echo '<div class="wysiwyg_content">';
				the_content();
				echo '</div>';
			}
			//詳細説明文
			if( get_field('detail') ){
				echo '<div class="clear">'.wp_kses_post(get_field('detail')).'</div>';
			}
			//担当者から
			if( have_rows('person') ){
				echo '<h2>担当者から</h2>';
				while( have_rows('person') ){
					the_row();
					//vars
					$title = get_sub_field('title');
					$name = get_sub_field('name');
					$content = get_sub_field('content');
					echo '<div class="tantou clear">';
					if($content){echo '<div>'.wp_kses_post($content).'</div>';}
					if($title or $name){
						echo '<p class="resname">';
						if($title){echo '<span>'.esc_html($title).'</span>';}
						if($name){echo esc_html($name);}
						echo '</p>';
					}
					echo '</div>';
				} 
			}
		?>
	</div><!-- .entry-content -->

	<?php
		// Author bio.
		if ( is_single() && get_the_author_meta( 'description' ) ) :
			get_template_part( 'author-bio' );
		endif;
	?>

	<footer class="entry-footer">
		<?php twentyfifteen_entry_meta(); ?>
		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
