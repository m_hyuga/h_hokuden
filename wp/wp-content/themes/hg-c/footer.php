<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<div id="footer">
	<div id="pagetop" class="over"><img src="<?php echo get_template_directory_uri(); ?>/common/images/common/img_pagetop.png" width="100%" alt=""></div>
	<div class="cf">
		<div class="fodeta1"><h5>北電技術コンサルタント株式会社</h5>&#12306;930-0858　富山県富山市牛島町13番15号<br>TEL/076-432-9936  FAX/076-432-4280</div>
		<div class="fodeta2"><h5>HGC建築設計事務所</h5>&#12306;930-0858　富山県富山市牛島町13番15号<br>TEL/076-442-1427<br>フリーダイヤル/0120-42-1428</div>
	</div>
	<p class="copy">Copyrights(c) 2010 北電技術コンサルタント株式会社 Corporation. All Rights Reserved.</p>
</div>
<?php wp_footer(); ?>

</body>
</html>
