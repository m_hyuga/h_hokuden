<?php
/**
 * The default template for displaying content of specialists single page.
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<script>
jQuery(window).on('load resize', function() {
jQuery('.works_box li').tile(4);
jQuery('.hgc_specialist li').tile(4);
});

</script>

<div class="hgc_content">
    <div class="hgc_page">
        <div class="hgc_tittle cf">
            <div class="hgc_left02">
                <h2><img src="common/images/hgc/logo01.png" alt="北陸電力グループ HGC建設設計事務所"></h2>
                <h3>スペシャリスト紹介</h3>
                <p>北陸トップクラスのスタッフの力が、安心と信頼を支える。</p>
            </div>
            <div class="content column2">
                <nav class="sideMenu">
                    <h3>スペシャリスト一覧</h3>
                    <ul>
                        <li><a href="">西田 秀幸</a></li>
                        <li><a href="">西田 秀幸</a></li>
                        <li><a href="">西田 秀幸</a></li>
                        <li><a href="">西田 秀幸</a></li>
                        <li><a href="">西田 秀幸</a></li>
                    </ul>
                </nav>
                <div class="rightCont">
                    <div class="entry-content">
                    <div class="hgc_left03">
                        <p><img src="http://hg-c.paddlechart.com/wp/wp-content/uploads/2015/09/sp-sugiyama1.jpg" class="attachment-post-thumbnail wp-post-image" alt="杉山　清久"></p>
                        </div>
                        <div class="hgc_right">
                        <section class="cf">
                            <h2>Name</h2>
                         <div class="list_txt">
                            <p><span>所長</span></p>
                            <p>杉山 清久</p>
                            <p><span>Sugiyama Kiyohisa</span></p>
                        </div>
                        </section>
                        <section class="cf">
                            <h2>Licence</h2>
                            <div class="list_txt">
                            <p>一級建築士<br>
                                構造設計一級建築士<br>
                                構造計算適合性判定員<br>
                                宅地建物取引主任<br>
                                建築積算士<br>
                                建築物環境衛生管理技術者</p>
                             </div>   
                                
                        </section>
                        <section class="cf">
                            <h2>Profile</h2>
                       <div class="list_txt">
                        <h3>[得意分野]</h3>
                        <p>大空間建造物</p>
                        <h3>[仕事へのこだわり]</h3>
                        <p>建物の安全は全てに優先され妥協は許されない</p>
                        <h3>[趣味]</h3>
                        <p>園芸、音楽ライブ鑑賞</p>
                        <h3>[一言メッセージ]</h3>
                        <p>スモールアーバンスペースに見るように小さな事でも思いやりの設計に心がけます。</p>
                       </div> 
                        </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /content -->







<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<h3>スペシャリスト一覧</h3>
		<ul>
		<?php
			$args = array(
				'posts_per_page'   => -1,
				'post_type'        => 'specialists'
			);
			$my_posts = get_posts( $args );
			echo '<div class="specialists-wrapper grid grid-fill">';
			foreach( $my_posts as $post ) {
				setup_postdata( $post );
				echo '<div class="grid_item grid_item-3 has-gutter">';
				echo '<a href="';
				the_permalink();
				echo '">';
				echo '<p>'.get_the_title().'</p>';
				echo '</a></div>';
			}
			wp_reset_postdata();

		?>
		</ul>

	<header class="entry-header">
		<?php
		// カスタムフィールド
		$name_en            = get_field('name_en');
		$position           = get_field('position');
		$licence            = get_field('licence');
		$speciality         = get_field('speciality');
		$kodawari           = get_field('kodawari');
		$hobby              = get_field('hobby');
		$message            = get_field('message');

		if ( is_single() ) :
			// 名前
			the_title( '<h1 class="page-title">', '</h1>' );
		else :
			the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
		endif;

		?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		// シングルページ
		if ( is_single() ):

			// サムネイル
			if ( has_post_thumbnail() ) {
				echo '<p>';
				the_post_thumbnail();
				echo '</p>';
			}

			// Name  ?>
			<section>
				<h2>Name</h2>
				<?php
				// 役職
				if ( $position ) {
					echo '<p>' . esc_html( $position ) . '</p>';
				}
				// 名前
				the_title( '<p>', '</p>' );
				// ローマ字名
				if ( $name_en ) {
					echo '<p>' . wp_kses_post( $name_en ) . '</p>';
				}
				?>
			</section>

			<?php
			// Licence
			if ( $licence ) : ?>
				<section>
					<h2>Licence</h2>
					<?php echo '<p>' . wp_kses_post( $licence ) . '</p>'; ?>
				</section>
			<?php
			endif;

			// Profile
			if ( $speciality || $kodawari || $hobby || $message ) : ?>
				<section>
					<h2>Profile</h2>
				</section>
				<?php
				// 得意分野
				if ( $speciality ) {
					echo '<h3>[得意分野]</h3>';
					echo '<p>' . wp_kses_post( $speciality ) . '</p>';
				}
				// 仕事へのこだわり
				if ( $kodawari ) {
					echo '<h3>[仕事へのこだわり]</h3>';
					echo '<p>' . wp_kses_post( $kodawari ) . '</p>';
				}
				// 趣味
				if ( $hobby ) {
					echo '<h3>[趣味]</h3>';
					echo '<p>' . wp_kses_post( $hobby ) . '</p>';
				}
				// 一言メッセージ
				if ( $message ) {
					echo '<h3>[一言メッセージ]</h3>';
					echo '<p>' . wp_kses_post( $message ) . '</p>';
				}
			endif;
		endif;
		?>
	</div>

	<footer class="entry-footer">
		<?php twentyfifteen_entry_meta(); ?>
		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
