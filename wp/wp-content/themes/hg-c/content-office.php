<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">

		<?php 
			if( have_rows('office') ){
				$str = '<table>';
				while( have_rows('office') ){
					the_row();
					$str .= '<tr>';
					//vars
					$office_name = get_sub_field('office_name');
					$office_detail = get_sub_field('office_detail');
					$map = get_sub_field('map');
					echo '<h2>'.esc_html($office_name).'</h2>';
					//echo '<div class="grid grid-fill">';
					//echo '<div class="grid_item grid_item-6 has-gutter">';
					echo '<div>'.wp_kses_post($office_detail).'</div>';
					//echo '</div>';
					//echo '<div class="grid_item grid_item-6 has-gutter"><div class="acf-map">';
					?>
					<div class="acf-map"><div class="marker" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>"><?php echo $map['address']; ?></div></div>
					<?php 
					//echo '</div></div>';
					//echo '</div>';
				}
			}
		 ?>
	</div><!-- .entry-content -->
	<footer class="entry-footer">
		<?php twentyfifteen_entry_meta(); ?>
		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
