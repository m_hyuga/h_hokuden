<?php
	if(!is_singular('proposal')){
		$args = array(
			'posts_per_page'   => esc_html(get_field('posts_per_page')),
			'category'         => esc_html(implode(',', get_field('posts_category')))
		);
	} else{
		$args = array(
			'posts_per_page'   => esc_html(get_sub_field('posts_per_page')),
			'category'         => esc_html(implode(',', get_sub_field('posts_category')))
		);
	}
	
	$news_posts = get_posts($args);
	if($news_posts){
	echo '
		<div class="infoArea">
			<h4>お知らせ<span>&emsp;/&emsp;</span><span class="news">news release</span></h4>
			<div class="newsList"><span>&rsaquo;</span><a href="'.esc_url( home_url() ).'/news/">お知らせ一覧</a></div>
			<dl class="cf">';
			foreach($news_posts as $p){
			$str .= '<dt>';
			$str .= mysql2date( 'Y年m月d日', $p->post_date );
			$cats = get_the_terms( $p->ID, 'category' );
			foreach( $cats as $c ){
				$str .= '<span class="icon'.$c->name.'">'.$c->name.'</span>';
			}
			$str .= '</dt>';
			$str .= '<dd><span><a href="'.$p->guid.'">';
			$str .= $p->post_title;
			$str .= '<div>&rsaquo;</div></a></span></dd>';
		} //end foreach
		echo $str;
		echo '</div>';
	} //end if


