<?php
/**
 * The sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
 	
?>
		<?php if ( is_home() or is_category() or is_singular('post') or is_year()) : ?>
		<nav class="widget">
			<div class="hentry">
				<ul class="sideMenu">
				<li class="cat-item-compensation"><span>カテゴリー</span>
					<ul class="children">
					<?php 
					$cat_all = get_terms( "category", "fields=all&get=all" );
					foreach($cat_all as $value):
					echo '<li><a href="'.get_category_link($value->term_id).'">'.$value->name.'</a></li>';
					endforeach; ?>
					</ul>
				</li>
				<li class="cat-item-compensation"><span>アーカイブ</span>
					<ul class="children">
					<?php wp_get_archives('type=yearly'); ?>
					</ul>
				</li>
				</ul>
			</div>
		</nav>

		
		<?php endif; ?>

			
		<?php if ( has_nav_menu( 'primary' ) ) : ?>
			<!--<nav class="main-navigation" role="navigation">
				<?php
					// Primary navigation menu.
					wp_nav_menu( array(
						'menu_class'     => 'nav-menu',
						'theme_location' => 'primary',
					) );
				?>
			</nav>--><!-- .main-navigation--> 
		<?php endif; ?>

		<?php
		// 事業案内
		if ( 'business' == get_post_type() or is_single('business') ):
		$page = get_post( get_the_ID() );
		$slug = $page->post_name;
		?>
		<nav class="widget">
		<div class="hentry">
		<ul class="sideMenu">
			<?php
			$terms = get_terms( 'business_tax' );
			?>
		<?php
		$businessllist = array(8, 12, 15,17,23);
		foreach ($businessllist as $business){
			$main_term = get_term( $business, 'business_tax' );
			$main_slug = $main_term->name; //カテゴリ名取得
			?>
				
			<li class="cat-item-<?php echo $main_slug; ?>"><span><?php echo $main_slug; ?></span>
				<ul>
				<?php
					$term_name = $term->name;
//						dump( $term_name );
					$term_slug = $term->slug;
//						dump( $term_slug );
				$args = array(
					'post_type' => 'business',
					'posts_per_page' => -1,
					'tax_query' => array(
						array(
							'taxonomy' => 'business_tax',
							'field' => 'id',
							'terms' =>  $business
						)
					)
				);
				$my_posts = get_posts( $args );
				//								dump( $my_posts );
				if ( $my_posts ) :
					foreach ( $my_posts as $post ):
						setup_postdata( $post ); ?>
									<li<?php
											$page = get_post( get_the_ID() );
											$listslug = $page->post_name;
											if ($listslug === $slug ){ echo ' class="current_page_item"';}
											?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
					<?php endforeach;
					wp_reset_postdata();
				endif; ?>
				</ul>
				</li>
		<?php
		}
		?>
				</ul>
		</div>
		</nav>
		<?php endif;

		// 実績紹介
		if ( 'works' == get_post_type() or is_single('works') ): ?>
		<nav class="widget">
		<div class="hentry">
		<ul class="sideMenu">
		<?php
				$worksllist = array(9,28,57,64,77);
				foreach ($worksllist as $works){
					$main_term = get_term( $works, 'classification' );
					$main_slug = $main_term->name; //カテゴリ名取得
					$main_slugname = $main_term->slug; //カテゴリスラッグ取得
					$category_link = get_term_link($works , 'classification');
					
					echo '<li class="cat-item-'.$main_slugname.'"><a href="'.esc_url( $category_link ).'">'.$main_slug.'</a>';
					echo '<ul class="children">';
					$args = array(
						'taxonomy' => 'classification',
						'child_of' => $works,
						'title_li' => ''
					);
					wp_list_categories( $args );
					echo '</ul>';
					echo '</li>';

				}

		?>
			</ul>
		</div>
		
		</nav>
		<?php endif;

		// 採用情報
		if ( 'recruit' == get_post_type() || 'senior' == get_post_type() ): ?>
		<nav class="widget">
		<div class="hentry">
			<ul class="sideMenu">
					<?php
						$posts_side = get_posts(array("post_type"=>'recruit','name'=>'recruit-top')); global $post; 
						if($posts_side): foreach($posts_side as $post): setup_postdata($post); $id = $post->ID;
							echo '<li class="page_item_has_children"><a href="'.get_the_permalink().'">'.get_the_title().'</a>';
								echo '<ul class="children">';
								$args = array(
									'post_type' => 'recruit',
									'title_li' => '',
									'child_of' => $post -> ID
								);
								wp_list_pages( $args );
								echo '</ul>';
							echo '</li>';
						endforeach; endif; wp_reset_postdata();
					?>
				<?php // 先輩が贈るひとこと ?>
				<li class="page_item_has_children<?php if(is_post_type_archive('senior')){ echo ' current_page_item';} ?>"><a href="<?php echo get_post_type_archive_link('senior'); ?>"><?php echo get_post_type_object('senior')->label; ?></a>
						<ul>
						<?php
						global $post;
						$page = get_post( get_the_ID() );
						$slug = $page->post_name;
						$args = array(
							'post_type' => 'senior',
							'orderby' => 'date',
							'order' => 'ASC',
							'posts_per_page' => -1
						);
						$my_posts = get_posts( $args );
						foreach( $my_posts as $post ) :
							setup_postdata( $post ); ?>
							<li<?php
											if (!is_post_type_archive()){
											$page = get_post( get_the_ID() );
											$listslug = $page->post_name;
											if ($listslug === $slug ){ echo ' class="current_page_item"';}
											};
											?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
						<?php endforeach;
						wp_reset_postdata(); ?>
						</ul>
				</li>
			</ul>
		</div>
		</nav>
		<?php endif;

		// 会社案内
		if ( 'company' == get_post_type() ): ?>
        
		<nav class="widget">
        <div class="hentry">
			<ul class="sideMenu">
        <li class="cat-item-compensation"><a href="<?php echo esc_url( home_url( '/' )); ?>company/company-top/">会社案内</a>
				<ul class="children">
				<?php
				$args = array(
					'post_type' => 'company',
					'title_li' => '',
          'child_of' => 40
				);
				wp_list_pages( $args );
				?>
				</ul>
				</li>
			</ul>
             </div>
		</nav>
       
		<?php endif;
			
		// 私たちの提案 - 中分類とその下のページをリスト表示
		$parent_id = $post->post_parent;
		if ( ('proposal' == get_post_type()) && ( $parent_id > 0 ) ):
		//親の親のID
		$grand_parent_id = get_post($parent_id) -> post_parent;
		//中分類のID
		if( $grand_parent_id > 0){
			//最下層postの場合
			$middle_post_id = $parent_id;
		} else {
			//中分類postの場合
			$middle_post_id = $post -> ID;
		}
		if (is_single('structural-design-p')){
		} else {
		 ?>
		<nav class="widget">
		<div class="hentry">
			<ul class="sideMenu">
				<?php
						if(is_parent_slug() === 'proposal-top'){
							$proposal = $post->ID;
						} else {
							$proposal = $post->post_parent;
						}
						$posts = get_posts(array("include"=>$proposal, "post_type"=>'proposal')); global $post; 
						if($posts): foreach($posts as $post): setup_postdata($post); $id = $post->ID;
							echo '<li class="cat-item-compensation"><a href="'.get_the_permalink().'">'.get_the_title().'</a>';
								echo '<ul class="children">';
								$args = array(
									'post_type' => 'proposal',
									'title_li' => '',
									'child_of' => $post -> ID
								);
								wp_list_pages( $args );
								echo '</ul>';
							echo '</li>';
						endforeach; endif; wp_reset_postdata();
				?>
			</ul>
			</div>
		</nav>
		<?php }; endif; ?>

		<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
			<div id="widget-area" class="widget-area" role="complementary">
				<?php dynamic_sidebar( 'sidebar-1' ); ?>
			</div><!-- .widget-area -->
		<?php endif; ?>
		