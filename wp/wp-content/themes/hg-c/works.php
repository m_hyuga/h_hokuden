<?php 
	if (!is_single('hgc-architects')){
		
		
	if(get_field('works')){
		$works_posts = get_field('works');
	} elseif(get_field('related_works')){
		$works_posts = get_field('related_works');
	} elseif(get_sub_field('works_posts')){
		$works_posts = get_sub_field('works_posts');
	}

	if($works_posts){
		$cnt_works = 0;
		echo '<table class="table4">';
		echo '<tr>';
		echo '<th class="jigyo">事業名</th><th class="hachu">年度</th><th class="nendo">発注者</th>';
		echo '</tr>';
		foreach($works_posts as $p){
			$cnt_works++;
			//vars
			$id = $p->ID;
			$url = get_permalink( $id );
			$title = get_the_title( $id );
			$fiscal_year = get_the_terms( $id, 'fiscal_year' );
			$client = get_the_terms( $id, 'client' );
			$eyecatch_url = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'thumbnail');
			//string
			$str = '<tr>';
			$str .= '<td>';
			if( get_field('index_link', $id) == 1 ){ //「一覧から詳細ページへリンクする」がチェックされている場合
				$str .= '<a href="';
				$str .= esc_url($url);
				$str .= '">';
			}
			$str .= esc_html($title);
			if( get_field('index_link', $id) == 1 ){
				$str .= '</a>';
			}
			$str .= '</td>';
						
			$str .= '<td>';
			// 年度・発注者
			if($fiscal_year){
				$count = 0;
				foreach($fiscal_year as $f){
					$str .= strtoupper(esc_html($fiscal_year[$count] -> name));
					$count++;
					if ($f === end($fiscal_year)) {} else {
						$str .= '、';
					}
				}
			}
			$str .= '</td>';
						
			$str .= '<td>';
			if($client){
				$str .= esc_html($client[0] -> name);
				$str .= "様";
			}
			$str .= '</td>';
			$str .= '</tr>';
			//echo
			echo $str;
			if (is_single('welfare-facilities')){
				if ($cnt_works == 2) {
					break;
				}
			}
		}
			echo '</table>';
	}

	} else {

			$works_posts = get_field('related_works');




	if($works_posts){
		foreach($works_posts as $p){
			//vars
				$id = $p->ID;
				$url = get_permalink( $id );
				$title = get_the_title( $id );
				$fiscal_year = get_the_terms( $id, 'fiscal_year' );
				$client = get_the_terms( $id, 'client' );
			$thumb_id = get_post_thumbnail_id($id);
			$eyecatch = wp_get_attachment_image_src($thumb_id, 'thumbnail');
			$eyecatch_url = $eyecatch[0];
			//string
			//string
				$str = '<li>';
				$str .= '<a href="'.esc_url($url).'" class="over">';
				if (!empty($eyecatch_url)){
					$str .= '<img src="'.esc_url($eyecatch_url).'" width="100%" alt="">';
				} else {
					$str .=  '<img src="'.get_template_directory_uri().'/common/images/hgc/dammy_works.jpg" width="100%">';
				}
						
				$str .= '<p>'.esc_html($title).'</p>';
				$str .= '</a>';
				$str .= '</li>';
			//echo
			echo $str;
		}
	}

}