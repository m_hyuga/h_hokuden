<?php
/**
 * Twenty Fifteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Twenty Fifteen 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 660;
}

/**
 * Twenty Fifteen only works in WordPress 4.1 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'twentyfifteen_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since Twenty Fifteen 1.0
 */
function twentyfifteen_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on twentyfifteen, use a find and replace
	 * to change 'twentyfifteen' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'twentyfifteen', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );
	add_image_size( 'index-thumb', 150, 100 );//added by mint

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'twentyfifteen' ),
		'social'  => __( 'Social Links Menu', 'twentyfifteen' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

	$color_scheme  = twentyfifteen_get_color_scheme();
	$default_color = trim( $color_scheme[0], '#' );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'twentyfifteen_custom_background_args', array(
		'default-color'      => $default_color,
		'default-attachment' => 'fixed',
	) ) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css', twentyfifteen_fonts_url() ) );
}
endif; // twentyfifteen_setup
add_action( 'after_setup_theme', 'twentyfifteen_setup' );

/**
 * Register widget area.
 *
 * @since Twenty Fifteen 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function twentyfifteen_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'twentyfifteen' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'twentyfifteen_widgets_init' );

if ( ! function_exists( 'twentyfifteen_fonts_url' ) ) :
/**
 * Register Google fonts for Twenty Fifteen.
 *
 * @since Twenty Fifteen 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function twentyfifteen_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Noto Sans, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Noto Sans font: on or off', 'twentyfifteen' ) ) {
		$fonts[] = 'Noto Sans:400italic,700italic,400,700';
	}

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Noto Serif, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Noto Serif font: on or off', 'twentyfifteen' ) ) {
		$fonts[] = 'Noto Serif:400italic,700italic,400,700';
	}

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Inconsolata, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'twentyfifteen' ) ) {
		$fonts[] = 'Inconsolata:400,700';
	}

	/*
	 * Translators: To add an additional character subset specific to your language,
	 * translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language.
	 */
	$subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'twentyfifteen' );

	if ( 'cyrillic' == $subset ) {
		$subsets .= ',cyrillic,cyrillic-ext';
	} elseif ( 'greek' == $subset ) {
		$subsets .= ',greek,greek-ext';
	} elseif ( 'devanagari' == $subset ) {
		$subsets .= ',devanagari';
	} elseif ( 'vietnamese' == $subset ) {
		$subsets .= ',vietnamese';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), '//fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Fifteen 1.1
 */
function twentyfifteen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentyfifteen_javascript_detection', 0 );

/**
 * Enqueue scripts and styles.
 *
 * @since Twenty Fifteen 1.0
 */
function twentyfifteen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentyfifteen-fonts', twentyfifteen_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.2' );

	// Load our main stylesheet.
//	wp_enqueue_style( 'twentyfifteen-style', get_stylesheet_uri() );
	
	// 追加スタイルシート.
	wp_enqueue_style( 'reset-style', get_template_directory_uri() . '/common/css/meyer.css' );
	wp_enqueue_style( 'common-style', get_template_directory_uri() . '/common/css/common.css' );
	wp_enqueue_style( 'common_new-style', get_template_directory_uri() . '/common/css/common_new.css' );
	if (is_front_page()){
	wp_enqueue_style( 'top-style', get_template_directory_uri() . '/common/css/top.css' );
	wp_enqueue_style( 'top_new-style', get_template_directory_uri() . '/common/css/top_new.css' );
	}
	if (is_single('welfare-facilities')){
	wp_enqueue_style( 'welfare-style', get_template_directory_uri() . '/common/css/proposal.css' );
	}
	if (is_single('hgc-architects') || is_singular('specialists')){
	wp_enqueue_style( 'hgc-style', get_template_directory_uri() . '/common/css/hgc.css' );
	}
	if (is_singular('recruit') || is_singular('senior') || 'senior' == get_post_type()){
	wp_enqueue_style( 'recruit-style', get_template_directory_uri() . '/common/css/recruit.css' );
	}
	if (is_page('49')){
	wp_enqueue_style( 'contact-style', get_template_directory_uri() . '/common/css/contact.css' );
	}
if (is_page('2519')){
	wp_enqueue_style( 'contact-style', get_template_directory_uri() . '/common/css/sitemap.css' );
	}
	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'twentyfifteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentyfifteen-style' ), '20141010' );
	wp_style_add_data( 'twentyfifteen-ie', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'twentyfifteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentyfifteen-style' ), '20141010' );
	wp_style_add_data( 'twentyfifteen-ie7', 'conditional', 'lt IE 8' );

	// 追加JS.
	wp_enqueue_script( 'common-js', get_template_directory_uri() . '/common/js/common.js' );
	wp_enqueue_script( 'webfont-js', '//webfont.fontplus.jp/accessor/script/fontplus.js?vnV4Ir4XKUw%3D&pm=1&aa=1' );
	
	wp_enqueue_script( 'twentyfifteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20141010', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	if (is_front_page() || is_single('hgc-architects') || is_singular('specialists') || is_post_type_archive('proposal')){
		wp_enqueue_script( 'tile-js', get_template_directory_uri() . '/common/js/jquery.tile.js' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentyfifteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20141010' );
	}

	wp_enqueue_script( 'twentyfifteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150330', true );
	wp_localize_script( 'twentyfifteen-script', 'screenReaderText', array(
		'expand'   => '<span class="screen-reader-text">' . __( 'expand child menu', 'twentyfifteen' ) . '</span>',
		'collapse' => '<span class="screen-reader-text">' . __( 'collapse child menu', 'twentyfifteen' ) . '</span>',
	) );
}
add_action( 'wp_enqueue_scripts', 'twentyfifteen_scripts' );

/**
 * Add featured image as background image to post navigation elements.
 *
 * @since Twenty Fifteen 1.0
 *
 * @see wp_add_inline_style()
 */
function twentyfifteen_post_nav_background() {
	if ( ! is_single() ) {
		return;
	}

	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );
	$css      = '';

	if ( is_attachment() && 'attachment' == $previous->post_type ) {
		return;
	}

	if ( $previous &&  has_post_thumbnail( $previous->ID ) ) {
		$prevthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $previous->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-previous { background-image: url(' . esc_url( $prevthumb[0] ) . '); }
			.post-navigation .nav-previous .post-title, .post-navigation .nav-previous a:hover .post-title, .post-navigation .nav-previous .meta-nav { color: #fff; }
			.post-navigation .nav-previous a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	if ( $next && has_post_thumbnail( $next->ID ) ) {
		$nextthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $next->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-next { background-image: url(' . esc_url( $nextthumb[0] ) . '); border-top: 0; }
			.post-navigation .nav-next .post-title, .post-navigation .nav-next a:hover .post-title, .post-navigation .nav-next .meta-nav { color: #fff; }
			.post-navigation .nav-next a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	wp_add_inline_style( 'twentyfifteen-style', $css );
}
add_action( 'wp_enqueue_scripts', 'twentyfifteen_post_nav_background' );

/**
 * Display descriptions in main navigation.
 *
 * @since Twenty Fifteen 1.0
 *
 * @param string  $item_output The menu item output.
 * @param WP_Post $item        Menu item object.
 * @param int     $depth       Depth of the menu.
 * @param array   $args        wp_nav_menu() arguments.
 * @return string Menu item with possible description.
 */
function twentyfifteen_nav_description( $item_output, $item, $depth, $args ) {
	if ( 'primary' == $args->theme_location && $item->description ) {
		$item_output = str_replace( $args->link_after . '</a>', '<div class="menu-item-description">' . $item->description . '</div>' . $args->link_after . '</a>', $item_output );
	}

	return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'twentyfifteen_nav_description', 10, 4 );

/**
 * Add a `screen-reader-text` class to the search form's submit button.
 *
 * @since Twenty Fifteen 1.0
 *
 * @param string $html Search form HTML.
 * @return string Modified search form HTML.
 */
function twentyfifteen_search_form_modify( $html ) {
	return str_replace( 'class="search-submit"', 'class="search-submit screen-reader-text"', $html );
}
add_filter( 'get_search_form', 'twentyfifteen_search_form_modify' );

/**
 * Implement the Custom Header feature.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 *
 * @since Twenty Fifteen 1.0
 */
require get_template_directory() . '/inc/customizer.php';

/**
 *
 */
function dump( $var ) {
	echo '<pre>';
	var_dump( $var );
	echo '</pre>';
}





/**
 * Custom post type カスタム投稿タイプ
 *
 */
add_action( 'init', 'create_post_type');
function create_post_type() {
	// business 事業案内 page型
	$args = array(
		'labels' => array(
			'name' => '事業案内'
		),
		'public' => true,
		'hierarchical' => false,
		'has_archive' => true,
		'supports' => array(
			'title',
			'editor',
			'author',
			'thumbnail',
			'excerpt',
			'revisions'
		)
	);
	register_post_type( 'business', $args );

	// works 実績紹介 post型
	$args = array(
		'labels' => array(
			'name' => '実績紹介'
		),
		'public' => true,
		'hierarchical' => false,
		'has_archive' => true,
		'supports' => array(
			'title',
			'editor',
			'author',
			'thumbnail',
			'excerpt',
			'revisions'
		)
	);
	register_post_type( 'works', $args );

	// proposal 私たちの提案 page型
	$args = array(
		'labels' => array(
			'name' => 'わたしたちの提案',
			'add_new_item' => 'わたしたちの提案を新規追加',
			'edit_item' => 'わたしたちの提案を編集',
			'new_item' => '新規',
			'view_item' => '表示',
			'search_items' => '検索'
		),
		'public' => true,
		'hierarchical' => true,
		'has_archive' => true,
		'supports' => array(
			'title',
			'editor',
			'author',
			'thumbnail',
			'excerpt',
			'revisions',
			'page-attributes'
		)
	);
	register_post_type( 'proposal', $args );

	// quality 品質への取り組み page型
	$args = array(
		'labels' => array(
			'name' => '品質への取り組み'
		),
		'public' => true,
		'hierarchical' => true,
		'has_archive' => true,
		'supports' => array(
			'title',
			'editor',
			'author',
			'thumbnail',
			'excerpt',
			'revisions',
			'page-attributes'
		)
	);
	register_post_type( 'quality', $args );

	// arch HGC建築設計事務所 page型
	$args = array(
		'labels' => array(
			'name' => 'HGC建築設計事務所'
		),
		'public' => true,
		'hierarchical' => true,
		'has_archive' => true,
		'supports' => array(
			'title',
			'editor',
			'author',
			'thumbnail',
			'excerpt',
			'revisions',
			'page-attributes'
		)
	);
	register_post_type( 'arch', $args );

	// specialists スタッフ紹介 post型
	$args = array(
		'labels' => array(
			'name' => 'スタッフ紹介'
		),
		'public' => true,
		'hierarchical' => false,
		'has_archive' => true,
		'supports' => array(
			'title',
			'editor',
			'author',
			'thumbnail',
			'excerpt',
			'revisions'
		)
	);
	register_post_type( 'specialists', $args );

	// recruit 採用情報 page型
	$args = array(
		'labels' => array(
			'name' => '採用情報'
		),
		'public' => true,
		'hierarchical' => true,
		'has_archive' => true,
		'supports' => array(
			'title',
			'editor',
			'author',
			'thumbnail',
			'excerpt',
			'revisions',
			'page-attributes'
		)
	);
	register_post_type( 'recruit', $args );

	// senior 先輩が贈るひとこと post型
	$args = array(
		'labels' => array(
			'name' => '先輩が贈るひとこと'
		),
		'public' => true,
		'hierarchical' => true,
		'has_archive' => true,
		'supports' => array(
			'title',
			'editor',
			'author',
			'thumbnail',
			'excerpt',
			'revisions'
		)
	);
	register_post_type( 'senior', $args );

	// company 会社案内 page型
	$args = array(
		'labels' => array(
			'name' => '会社案内'
		),
		'public' => true,
		'hierarchical' => true,
		'has_archive' => true,
		'supports' => array(
			'title',
			'editor',
			'author',
			'thumbnail',
			'excerpt',
			'revisions',
			'page-attributes'
		)
	);
	register_post_type( 'company', $args );

	// pamphlets パンフレット post型
	$args = array(
		'labels' => array(
			'name' => 'パンフレット'
		),
		'public' => true,
		'hierarchical' => false,
		'has_archive' => true,
		'menu_position' => 5,
		'add_new_item' => '新規パンフレットを追加',
		'menu_icon' => 'dashicons-media-document',
		'supports' => array(
			'title',
			'editor',
			'author',
			'revisions'
		)
	);
	register_post_type( 'pamphlets', $args );

}


/**
 * Custom Taxonomy カスタム投稿タイプ
 *
 */
add_action( 'init', 'create_taxonomy' );
function create_taxonomy() {
	// business_tax 事業案内用分類
	$args = array(
		'hierarchical' => true,
		'labels' => array(
			'name' => '事業案内の分類'
		),
	);
	register_taxonomy(
		'business_tax',
		array(
			'business'
		),
		$args
	);

	// classification 実績紹介用業務分類
	$args = array(
		'hierarchical' => true,
		'labels' => array(
			'name' => '業務分類'
		),
	);
	register_taxonomy(
		'classification',
		array(
			'works'
		),
		$args
	);

	// fiscal_year 実績紹介用年度
	$args = array(
		'hierarchical' => true,
		'labels' => array(
			'name' => '年度'
		),
	);
	register_taxonomy(
		'fiscal_year',
		array(
			'works'
		),
		$args
	);

	// client 実績紹介用発注者名
	$args = array(
		'hierarchical' => true,
		'labels' => array(
			'name' => '発注者名'
		),
	);
	register_taxonomy(
		'client',
		array(
			'works'
		),
		$args
	);
}

//sidebarの有無をbodyのclassに追加

function my_class_names($classes) {

	$post_type = get_post_type();
	if( is_single() ){
		global $post;
		$ID = $post->ID;
		$type_name = $post_type.'_page_'.$ID;
		$classes[] = $type_name;
	}
	if( (is_front_page()) || (is_archive() && ($post_type == 'business')) || (get_field('has_sidebar') == 'サイドバー無')	 ){
		$classes[] = 'no-sidebar';
	} else {
		$classes[] = 'with-sidebar';
	}
		return $classes;
}
add_filter('body_class', 'my_class_names');

//google-maps.js を que に追加
function my_theme_add_scripts() {
	//事務所一覧の場合のみ
	if(is_single('office') || is_single('organization')){
		wp_enqueue_script( 'google-map', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false', array(), '3', true );
		wp_enqueue_script( 'google-map-init', get_template_directory_uri() . '/js/google-maps.js', array('google-map', 'jquery'), '0.1', true );
	}
}

add_action( 'wp_enqueue_scripts', 'my_theme_add_scripts' );

//管理画面のcssに追記する
function my_admin_style() {
  echo '<style>.wp-admin .acf-file-uploader .file-wrap {min-height: 320px;}.wp-admin .acf-file-uploader .file-info {margin-left: 250px;}</style>';
}
add_action('admin_print_styles', 'my_admin_style');



// -----------検索フォーム
function my_search_form( $form ) {
 
    $form = '<form role="search" method="get" id="searchform" action="'.home_url( '/' ).'" >
		<label class="screen-reader-text" for="s"></label>
		<input type="text" value="' . get_search_query() . '" placeholder="サイト内検索" name="s" id="s" class="keywords">
		<button class="searchBtn"><img src="'.  get_template_directory_uri() .'/common/images/common/img_search.png" width="100%" alt=""></button>
    </form>';
 
    return $form;
}
 
add_filter( 'get_search_form', 'my_search_form' );



// サイドバーのカテゴリーリスト整形
function add_cat_slug_class( $output, $args ) {
    $regex = '/<li class="cat-item cat-item-([\d]+)[^"]*">/';
    $taxonomy = isset( $args['taxonomy'] ) && taxonomy_exists( $args['taxonomy'] ) ? $args['taxonomy'] : 'category';
     
    preg_match_all( $regex, $output, $m );
     
    if ( ! empty( $m[1] ) ) {
        $replace = array();
        foreach ( $m[1] as $term_id ) {
            $term = get_term( $term_id, $taxonomy );
            if ( $term && ! is_wp_error( $term ) ) {
                $replace['/<li class="cat-item cat-item-' . $term_id . '("| )/'] = '<li class="cat-item cat-item-' . $term_id . ' cat-item-' . esc_attr( $term->slug ) . '$1';
            }
        }
        $output = preg_replace( array_keys( $replace ), $replace, $output );
    }
    return $output;
}
add_filter( 'wp_list_categories', 'add_cat_slug_class', 10, 2 );

add_editor_style("editor-style.css");
remove_filter( 'get_the_excerpt', 'wpautop' );
add_filter( 'get_the_excerpt', 'nl2br' );




function is_parent_slug() {
  global $post;
  if ($post->post_parent) {
    $post_data = get_post($post->post_parent);
    return $post_data->post_name;
  }
}


function isLast(){
    global $wp_query;
    return ($wp_query->current_post+1 === $wp_query->post_count);
}



// アーカイブ表記に「年」を追加
function my_archives_link($html){
if(preg_match('/[0-9]+?<\/a>/', $html))
$html = preg_replace('/([0-9]+?)<\/a>/', '$1年</a>', $html);
if(preg_match('/title=[\'\"][0-9]+?[\'\"]/', $html))
$html = preg_replace('/(title=[\'\"][0-9]+?)([\'\"])/', '$1年$2', $html);
return $html;
}
add_filter('get_archives_link', 'my_archives_link', 10);


// お問い合わせ：ラジオボタンを必須に
add_action( 'wpcf7_init', 'wpcf7_add_shortcode_radio_required' );
 
function wpcf7_add_shortcode_radio_required() {
	wpcf7_add_shortcode( array( 'radio*' ), 
		'wpcf7_checkbox_shortcode_handler', true );
}
 
add_filter( 'wpcf7_validate_radio*', 'wpcf7_checkbox_validation_filter', 10, 2 );


/* テンプレートフォルダのパスをショートコードに登録
--------------------------------------------------------- */
function shortcode_templateurl() {
    return get_bloginfo('template_url');
}
add_shortcode('template_url', 'shortcode_templateurl');


function homepage_url() {
    return home_url();
}
add_shortcode('home_url', 'homepage_url');



add_filter (  'wp_feed_cache_transient_lifetime' , 'return_1800' );
$feed = fetch_feed( $feed );
remove_filter( 'wp_feed_cache_transient_lifetime' , 'return_1800' );
function return_1800(){
    return 0;
}

/* PDFからサムネイルを生成 
--------------------------------------------------------- */
function capture_heartrails_show_pdf_thumbnail( $pdf_file_id ){
	// idからPDFファイルのurlを取得
	$pdf_file_url = wp_get_attachment_url( $pdf_file_id );
	$str = '<img style="border: 1px solid #ccc;" src="http://capture.heartrails.com/122x173/shorten?'.$pdf_file_url.'">';
	echo $str;
}
