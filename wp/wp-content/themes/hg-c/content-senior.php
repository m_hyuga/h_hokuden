<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header achList_detail">
		<?php
		// カスタムフィールド
		$updated_date       = get_field('updated_date');
		$affiliation        = get_field('affiliation');
		$graduate           = get_field('graduate');
		$my_job_title       = get_field('my_job_title');
		$my_job             = get_field('my_job');

		

		if ( is_single() ) :
			// カスタムフィールド
			$episode_title      = get_field('episode_title');
			$episode            = get_field('episode');
			$reason_title       = get_field('reason_title');
			$reason             = get_field('reason');
			$career             = get_field('career');
			$job_cagtegory      = get_field('job_category');
			$description        = get_field('description');
			$style              = get_field('style');
			$person             = get_field('person');
			$acquisition         = get_field('acquisition');
			$personality        = get_field('personality');
			$advice             = get_field('advice');
    
        // サムネイル
		if ( has_post_thumbnail() ) {
			echo '<p class="img_left">';
			the_post_thumbnail();
			echo '</p>';
		}
		// 所属
		if ( $affiliation ) {
			echo '<p class="affiliation_title">' . esc_html( $affiliation ) . '</p>';
		}
			// 名前
			the_title( '<p class="rec">', '</p>' );
		else :
			the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
		endif;

		// 出身
		if ( $graduate ) {
			echo '<p class="graduate_title"><img src="'. get_template_directory_uri().'/common/images/recruit/titile_graduate.jpg" alt="出身" width＝”5%”>'. esc_html( $graduate ) . '</p>';
		}
		
		?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		// シングルページ
		if ( is_single() ):
			// これが私の仕事
			if ( $my_job_title || $my_job ) : ?>
				<h2>これが私の仕事</h2>
				<?php
				if ( $my_job_title ) {
					echo '<h3>' . esc_html( $my_job_title ) . '</h3>';
				}
				if ( $my_job ) {
					echo '<div class="clear">' . wp_kses_post( $my_job ) . '</div>';
				}
			endif;

			// だからこの仕事が好き！　エピソード
			if ( $episode_title || $episode ) : ?>
				<h2>だからこの仕事が好き！一番うれしかったことにまつわるエピソード</h2>
				<?php
				if ( $episode_title ) {
					echo '<h3>' . esc_html( $episode_title ) . '</h3>';
				}
				if ( $episode ) {
					echo '<div class="clear">' . wp_kses_post( $episode ) . '</div>';
				}
			endif;

			// ズバリ！私がこの会社を選んだ理由
			if ( $my_job_title || $my_job ) : ?>
				<h2>ズバリ！私がこの会社を選んだ理由</h2>
				<?php
				if ( $reason_title ) {
					echo '<h3>' . esc_html( $reason_title ) . '</h3>';
				}
				if ( $reason ) {
					echo '<div class="clear">' . wp_kses_post( $reason ) . '</div>';
				}
			endif;

			// これまでのキャリア
			if ( $career ) {
				echo '<h2>これまでのキャリア</h2>';
				echo '<div class="clear">' . wp_kses_post( $career ) . '</div>';
			}

			// この仕事のポイント
			if ( $job_cagtegory || $description || $style || $person || $acquisition || $personality ): ?>
				<h2>この仕事のポイント</h2>
				<dl>
					<?php
					if ( $job_cagtegory ) {
						echo '<dt>職種系統</dt>';
						echo '<dd>' . esc_html( $job_cagtegory ) . '</dd>';
					}
					if ( $description ) {
						echo '<dt>仕事の中身</dt>';
						echo '<dd>' . esc_html( $description ) . '</dd>';
					}
					if ( $style ) {
						echo '<dt>仕事のスタイル</dt>';
						echo '<dd>' . esc_html( $style ) . '</dd>';
					}
					if ( $person ) {
						echo '<dt>仕事でかかわる人</dt>';
						echo '<dd>' . esc_html( $person ) . '</dd>';
					}
					if ( $acquisition ) {
						echo '<dt>仕事で身につくもの</dt>';
						echo '<dd>' . esc_html( $acquisition ) . '</dd>';
					}
					if ( $personality ) {
						echo '<dt>特に向いている性格</dt>';
						echo '<dd>' . esc_html( $personality ) . '</dd>';
					}
					?>
				</dl>

			<?php endif;

			// 先輩からの就職活動アドバイス！
			if ( $advice ) {
				echo '<h2>先輩からの就職活動アドバイス</h2>';
				echo '<div class="clear">' . wp_kses_post( $advice ) . '</div>';
			}
		endif;
		?>
	</div>

	<footer class="entry-footer">
		<?php twentyfifteen_entry_meta(); ?>
		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
