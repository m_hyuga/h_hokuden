<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<script>
jQuery(window).on('load resize', function() {
	jQuery('.works_box li').tile(4);
	jQuery('.hgc_specialist li').tile(6);
	jQuery('.hgc_middle li').tile(2);
});

</script>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="hgc_content">
<div class="hgc_top">
<div class="hgc_tittle cf">
<div class="hgc_left">
<h2><img src="<?php echo get_template_directory_uri(); ?>/common/images/hgc/logo01.png" alt="北陸電力グループ HGC建設設計事務所"></h2>

<h3>
<span>Message</span>
北陸トップクラスの豊富なスタッフが、<br>高い技術力で、あなたのイメージを<br>鮮やかに実現します。<br>
</h3>

</div>

<img src="<?php echo get_template_directory_uri(); ?>/common/images/hgc/man.png" alt="" class="flr">

</div>
<div class="hgc_link">
<div class="hgc_middle">

<ul class="cf">
		<?php
			/* =========
			 重点事業分野訴求
			===== */
			if(get_field('related_links')){
				$my_posts = get_field('related_links');
				foreach($my_posts as $p){
					//vars
					$id = $p->ID;
					$url = get_permalink( $id );
					$title = get_the_title( $id );
					//画像(返り値は「画像ID」)
								$attachment_id = get_field('lead_images02', $id);
								$size = "full"; // (thumbnail, medium, large, full or custom size)
								$image = wp_get_attachment_image_src( $attachment_id, $size );
					//string
					$str = '<li><span class="images">';
					if (!empty($image)){
						$str .= '<img src="'.$image[0].'" width="100%"/>';
					}
					$str .= '</span><span class="txt"><h5>';
					$str .= esc_html($title);
					$str .= '</h5><p>'.esc_html(get_field('lead_sentence', $id)).'</p><a href="';
					$str .= esc_url($url);
					$str .= '" class="over">';
					$str .= '<img src="'.get_template_directory_uri().'/common/images/common/arrow_gray.png" width="100%" alt="">詳細をみる</a>';
					$str .= '</span></li>';
					//echo
					echo $str;
				}
			}
			?>
	</ul>
      </div>
    <div class="hgc_works">
    <div class="works_ttl cf">
    <h4>施工事例<span>works</span></h4>
    <p>子どもやお年寄りに、そして環境にやさしい施設づくりに、<br>数々の実績と信頼を活かします。</p>
    </div>
    
		<ul class="works_box cf">
    <?php
				get_template_part('works');			
		?>
    </ul>
    
    </div>
    
    
  
</div>
</div>
<!--
<div class="business">
		<h1 class="page-title">事業案内</h1>
		<div class="business_inner cf">
			<?php
			$term_cnt = 0;
			$terms = get_terms( 'business_tax' );
			foreach( $terms as $term ):
			$term_slug = $term->slug;
			$term_cnt++;
			if ($term_cnt == 1){
				echo '<div class="fll">';
			}
			if ($term_cnt == 3){
				echo '<div class="flr">';
			}
			?>
			<h4 class="ternid_<?php echo $term->term_id; ?>"><?php echo $term->name; ?></h4>
				<ul>
				<?php
				$args = array(
					'post_type' => 'business',
					'posts_per_page' => -1,
					'tax_query' => array(
						array(
							'taxonomy' => 'business_tax',
							'field' => 'slug',
							'terms' => $term_slug
						)
					)
				);
				$my_posts = get_posts( $args );
				//								dump( $my_posts );
				if ( $my_posts ) :
					foreach ( $my_posts as $post ):
						setup_postdata( $post ); ?>
						<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
					<?php endforeach;
					wp_reset_postdata();
				endif; ?>
				</ul>
				<?php
				if ($term_cnt == 2 or $term_cnt == 5){
					echo '</div>';
				}
				 endforeach; ?>
			</div>
		</div>
	</div>-->

		<div class="hgc_specialist">
    
    
		<?php			
			/* =========
			 スタッフ紹介 一覧
			===== */
			echo '<h4>スタッフ紹介<span>staff</span></h4>';
			echo '<ul class="cf">';
			$args = array(
				'posts_per_page'   => -1,
				'post_type'        => 'specialists'
			);
			$my_posts = get_posts( $args );
			foreach( $my_posts as $post ) {
				echo '<li>';
				setup_postdata( $post );
				echo '<a href="';
				the_permalink();
				echo '" class="over">';
				$image_id = get_post_thumbnail_id();
				$image_url = wp_get_attachment_image_src($image_id, true);
				if (!empty($image_url)){
					echo  '<img src="'.$image_url[0].'" width="100%">';
				} else {
					echo  '<img src="'.get_template_directory_uri().'/common/images/hgc/dammy_staff.jpg" width="100%">';
				}
				echo '<p><span class="ttl">'.esc_html(get_field('position')).'</span><span class="name">'.get_the_title().'</span><span class="color"> '.esc_html(get_field('name_en')).'</span></p>';
				echo '</a></li>';
			}
			wp_reset_postdata();
			echo '</ul>';
		?>
<div class="centerCont">
	<footer class="entry-footer">
		<?php twentyfifteen_entry_meta(); ?>
		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</div>
	</div>
</div><!-- /content -->



	</div><!-- .entry-content -->


</article><!-- #post-## -->
