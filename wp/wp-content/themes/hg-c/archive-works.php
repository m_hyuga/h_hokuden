<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        <div class="mainImg_bg">
		<p>実績紹介</p><div class="mainImg"><img src="<?php echo get_template_directory_uri(); ?>/common/images/common/midashi/ttl_jisseki.jpg" width="100%" alt="実績紹介"></div>
        </div>
		<?php	get_template_part( 'content-pan' );?>
		<div class="content column2">
		<?php get_sidebar(); ?>
		<div class="rightCont"><!-- 右コンテンツここから -->
		<?php if ( have_posts() ) : ?>

			<header class="page-header">
        
            
				<h1 class="page-title">実績紹介</h1>
				<?php
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->
			<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();
			$all_flag = 0;
			if( get_field('index_link') == 1 ){ $all_flag = 1; };
			if ( has_post_thumbnail() ) { $all_flag = 3; };
			?>
			<section class="index-article">
				<?php
				if ($all_flag == 3){
				 echo '<div class="cf">';
				 echo '<div class="fll">';
				}
				//業務分類
				$classification = get_the_terms( $post -> post_id, 'classification' );
				if($classification){
					echo '<p class="classification">';
					$count = 0;
					foreach($classification as $c){
						$parent_id = $classification[$count] -> parent;
						$cat = get_term_by('id',$parent_id,'classification');
						echo '<span class="id_'.esc_html($cat->slug).'">'.esc_html($classification[$count] -> name).'</span>';
						$count++;
					}
					echo '</p>';
				}
				//タイトル
				if( get_field('index_link') == 1 ){
					echo '<h2><a href="'.get_the_permalink().'">'.get_the_title().'</a></h2>';
				} else {
					echo '<h2>'.get_the_title().'</h2>';
				}
				//本文抜粋
				$theContentForPreSingle = mb_substr(strip_tags($post-> post_content), 0, 70);
				$theContentForPreSingle = preg_replace('/\［caption(.+?)\/caption\］/','',$theContentForPreSingle);
				$theContentForPreSingle = preg_replace('/\［gallery(.+?)\］/','',$theContentForPreSingle);
				echo '<div class="txt">';
				echo $theContentForPreSingle;
				if(mb_strlen($theContentForPreSingle) >= 70){echo '...';};
				echo '</div>';
				
				if ($all_flag == 1){
				 echo '<div class="cf">';
				 echo '<div class="fll">';
				}
				echo '<p class="fiscal">';
				//年度
				$fiscal_year = get_the_terms( $post -> post_id, 'fiscal_year' );
				if($fiscal_year){
					$count = 0;
					foreach($fiscal_year as $f){
						echo '<span>'.esc_html($fiscal_year[$count] -> name).'</span>';
						$count++;
					}
				}
				//発注者名
				$client = get_the_terms( $post -> post_id, 'client' );
				//年度・発注者両方ある時' / '
				if(!empty($fiscal_year) and !empty($client)){
					echo ' / ';
				}
				if($client){
					echo '<span>'.esc_html($client[0] -> name).'</span>';
				}
				echo '</p>';
				if ($all_flag != 0 ){
				 echo '</div>';
				 echo '<div class="flr">';
				 	// サムネイル
					if ( has_post_thumbnail() ) {
						$image_id = get_post_thumbnail_id();
						$image_url = wp_get_attachment_image_src($image_id, true);
						?><p style="background-image:url('<?php
						echo  $image_url[0];
						?>')"></p><?php
					}
					//詳しくはこちら
					if( get_field('index_link') == 1 ){
						echo '<div class="btn_deta">';
						echo '<a href="';
						the_permalink();
						echo '" class="over"><span>&#155;</span>詳しくはこちら</a></div>';
					}
					echo '</div>';
					echo '</div>';
				}
				?>
			</section>
			<?php
			// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( '', 'twentyfifteen' ),
				'next_text'          => __( '', 'twentyfifteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'twentyfifteen' ) . ' </span>',
				'mid_size' => 3,
			) );

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>
		</div><!-- /右コンテンツここまで -->
		</div>
		</main><!-- .site-main -->
	</section><!-- .content-area -->
<?php get_footer(); ?>
