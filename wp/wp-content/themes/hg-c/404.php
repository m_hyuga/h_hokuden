<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<div class="content column1">
		<div class="centerCont">
			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentyfifteen' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p>申し訳ありません。ページが見つかりませんでした。</p>
					<br>
					<div class="search">
					<?php
					// 検索フォームを出力する
					get_search_form();
					// 検索フォームを代入する
					$search_form = get_search_form(false); ?>
					</div>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- .site-main -->
	</div><!-- .content-area -->
	</div><!-- .content-area -->
	</div><!-- .content-area -->
	</section>

<?php get_footer(); ?>
