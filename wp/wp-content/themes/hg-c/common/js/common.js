jQuery(function(){

//発火
pullDown();
pageTop();
spMenuSub();
spMenuBtn();
// sideNav();
sideNavFoot();
footMenuAc(); //フッタメニュのアコーディオン
recruitApp();
hoverOver();

if(jQuery('#top').size()) {
	//TOPのみの処理
	topAbout();
} else {
	//TOP以外のみの処理
	acMenu();
}


//アコーディオンメニュ
function acMenu() {
	jQuery('#acMenu dt').on('click', function(){
		jQuery(this).next().slideToggle();
		jQuery(this).toggleClass('onclick');
		if(jQuery(this).hasClass('onclick')) {
			jQuery(this).children('.arrow').children('img').attr('src', '/wp/wp-content/themes/hg-c/common/images/common/btn_top.png');
		} else {
			jQuery(this).children('.arrow').children('img').attr('src', '/wp/wp-content/themes/hg-c/common/images/common/btn_bottom.png');
		}
	});
}

function pullDown() {
	jQuery('.pulldownMenu').hover(function(){
		jQuery('.innerMenu').stop(true,false).fadeIn();
	}, function(){
		jQuery('.innerMenu').stop(true,false).fadeOut();
	});
}

function spMenuBtn() {
	jQuery(window).on('load resize', function(){
		var btnWidth = jQuery('#spBtn').width();
		jQuery('#spBtn').height(btnWidth);
	});
	jQuery('#spBtn').on('click', function(){
		jQuery(this).toggleClass('open');//押されたらクラス持たせる
		if(jQuery(this).hasClass('open')) {
			jQuery('#spMenu').stop(true,false).fadeIn();
		} else {
			jQuery('#spMenu').stop(true,false).fadeOut();
		}
	});
}
function spMenuSub() {
	jQuery('.spInnerBtn').on('click', function(){
		jQuery(this).toggleClass('subOp');//押されたらクラス持たせる
		if(jQuery(this).hasClass('subOp')) {
			jQuery('#subInnerMenu').stop(true,false).fadeIn();
		} else {
			jQuery('#subInnerMenu').stop(true,false).fadeOut();
		}
	});
}

function pageTop() {
	jQuery('#pagetop').on('click', function(){
		jQuery('body, html').animate({ scrollTop: 0 }, 500);
	});
}

function sideNavFoot() {
	jQuery(window).on('load resize', function(){
		if(jQuery(window).width() <= 768) {
			jQuery('.sideMenu').prependTo('#footer');
			footMenuBtn();
		} else {
			jQuery('.sideMenu').prependTo('.column2');
			jQuery('#spMenu').css({'display':'none'});
			jQuery('#spBtn.open').removeClass('open');
			jQuery('.btn_elOpCl,.ttl_Fsub').remove();
		}
	});
}
function footMenuBtn() {
	if(jQuery('#footer .sideMenu').size() == true && jQuery('#footer .sideMenu .btn_elOpCl').size() == false) { //footerの中にsideMenuがあり、且つ開閉ボタンが無い場合以下実行
		if(jQuery('ul.sideMenu>li').children('ul').size()) { //ul.sideMenuの直下liの中にulがいれば
			jQuery('ul.sideMenu>li').addClass('elOpCl'); //ulがいる親liに開閉用Class追加
			jQuery('.elOpCl').append('<span class="btn_elOpCl"><img src="https://www.hg-c.co.jp/wp/wp-content/themes/hg-c/common/images/common/sp_innerBtn.jpg" width="100%" alt></span>'); //開閉用画像追加
			jQuery('#footer .sideMenu').prepend('<li class="ttl_Fsub">サブメニュー</li>'); //開閉用画像追加
		}
	}
}
function footMenuAc() {
	jQuery(document).on('click', '.btn_elOpCl', function() { //btn_elOpClがクリックされたら
		jQuery(this).parent().children('ul').slideToggle();
		jQuery(this).toggleClass('op_on');
	});
	jQuery(window).on('load', function() {
		jQuery('.current_page_item').parent('ul').css({'display':'block'});
		jQuery('.current_page_item').parent('ul').parent('li').children('.btn_elOpCl').addClass('op_on');
	});
}

// function sideNav() {
// 	jQuery(window).on('load resize', function(){
// 		if(jQuery(window).width() <= 768) {
// 			jQuery('.sideMenu').appendTo('#spMenu');
// 		} else {
// 			jQuery('.sideMenu').prependTo('.column2');
// 			jQuery('#spMenu').css({'display':'none'});
// 			jQuery('#spBtn.open').removeClass('open');
// 		}
// 	});
// }

function topAbout() {
	jQuery('.about li a,.hgc_area ul li a').hover(function(){
		jQuery(this).children('.hover_on').stop(true,false).fadeIn(700);
	}, function(){
		jQuery(this).children('.hover_on').stop(true,false).fadeOut(400);
	});
}

function recruitApp() {
	jQuery('.appArea li a').hover(function(){
		jQuery(this).children('.hover_on').stop(true,false).fadeIn(700);
	}, function(){
		jQuery(this).children('.hover_on').stop(true,false).fadeOut(400);
	});
}



function hoverOver() {
	jQuery(".over") .hover(function(){
			 jQuery(this).stop().animate({'opacity' : '0.6'}, 180); // マウスオーバーで透明度を30%にする
		},function(){
			 jQuery(this).stop().animate({'opacity' : '1'}, 100); // マウスアウトで透明度を100%に戻す
	});
}


});