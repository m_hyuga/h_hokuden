<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php

		// Post thumbnail.
		if (!is_single('small-hydroelectric-generation')){
			if(has_post_thumbnail()){
				echo '<div class="main_thumb">';
				the_post_thumbnail('full');
				echo '</div>';
			}
		}
	?>

	<header class="entry-header">
		<?php
		if (!is_single('welfare-facilities')){
			if (is_single('small-hydroelectric-generation')){
				the_title( '<h2>', '</h2>' );
			}  else {
				the_title( '<h1>', '</h1>' );
			}
		} 
		?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			$post_slug = attribute_escape( $post->post_name );
			
			/* =========
			 介護福祉トップ
			===== */

			/* =========
			 柔軟コンテンツ
			===== */
				get_template_part('flexible');
			
			echo '</div><!-- .entry-content -->';
				
		?>

	

	<footer class="entry-footer">
		<?php twentyfifteen_entry_meta(); ?>
		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
