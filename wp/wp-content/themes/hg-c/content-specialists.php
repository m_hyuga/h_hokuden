<?php
/**
 * The default template for displaying content of specialists single page.
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<script>
jQuery(window).on('load resize', function() {
jQuery('.works_box li').tile(4);
jQuery('.hgc_specialist li').tile(4);
});

</script>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<div class="hgc_content">
    <div class="hgc_page">
        <div class="hgc_tittle cf">
            <div class="hgc_left02">
                <h2><img src="<?php echo get_template_directory_uri(); ?>/common/images/hgc/logo01.png" alt="北陸電力グループ HGC建設設計事務所"></h2>
                <h3>スタッフ紹介</h3>
                <p>北陸トップクラスのスタッフの力が、安心と信頼を支える。</p>
            </div>
            <div class="content column2">
                <nav class="sideMenu">
                    <h3>スタッフ一覧</h3>
                    <ul>
										<?php
											$args = array(
												'posts_per_page'   => -1,
												'post_type'        => 'specialists'
											);
											$my_posts = get_posts( $args );
											foreach( $my_posts as $post ) {
												echo '<li>';
												setup_postdata( $post );
												echo '<a href="';
												the_permalink();
												echo '">';
												echo get_the_title();
												echo '</a></li>';
											}
											wp_reset_postdata();
								
										?>
                    </ul>
                </nav>
                <div class="rightCont">
                    <div class="entry-content">
												<?php
												// カスタムフィールド
												$name_en            = get_field('name_en');
												$position           = get_field('position');
												$licence            = get_field('licence');
												$speciality         = get_field('speciality');
												$kodawari           = get_field('kodawari');
												$hobby              = get_field('hobby');
												$message            = get_field('message');
										
										
												?>
											</header><!-- .entry-header -->
										
											<div class="entry-content">
												<?php
												// シングルページ
												if ( is_single() ):
										
													// サムネイル
													echo '<div class="hgc_left03">';
													if ( has_post_thumbnail() ) {
														echo '<p>';
														the_post_thumbnail();
														echo '</p>';
													}
													echo '
													</div>
													<div class="hgc_right">
													';
										
													// Name  ?>
													<section class="cf">
														<h2>Name</h2>
														<div class="list_txt">
														<?php
														// 役職
														if ( $position ) {
															echo '<p><span>' . esc_html( $position ) . '</span></p>';
														}
														// 名前
														the_title( '<p>', '</p>' );
														// ローマ字名
														if ( $name_en ) {
															echo '<p><span>' . wp_kses_post( $name_en ) . '</span></p>';
														}
														?>
														</div>
													</section>
										
													<?php
													// Licence
													if ( $licence ) : ?>
														<section class="cf">
															<h2>Licence</h2>
															<div class="list_txt">
															<?php echo '<p>' . wp_kses_post( $licence ) . '</p>'; ?>
															</div>
														</section>
													<?php
													endif;
										
													// Profile
													if ( $speciality || $kodawari || $hobby || $message ) : ?>
														<section class="cf">
															<h2>Profile</h2>
														<div class="list_txt">
														<?php
														// 得意分野
														if ( $speciality ) {
															echo '<h3>[得意分野]</h3>';
															echo '<p>' . wp_kses_post( $speciality ) . '</p>';
														}
														// 仕事へのこだわり
														if ( $kodawari ) {
															echo '<h3>[仕事へのこだわり]</h3>';
															echo '<p>' . wp_kses_post( $kodawari ) . '</p>';
														}
														// 趣味
														if ( $hobby ) {
															echo '<h3>[趣味]</h3>';
															echo '<p>' . wp_kses_post( $hobby ) . '</p>';
														}
														// 一言メッセージ
														if ( $message ) {
															echo '<h3>[一言メッセージ]</h3>';
															echo '<p>' . wp_kses_post( $message ) . '</p>';
														}
													endif;
												endif;
												?>

                       </div> 
                        </section>
												<footer class="entry-footer">
													<?php twentyfifteen_entry_meta(); ?>
													<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
												</footer><!-- .entry-footer -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- /content -->


</article><!-- #post-## -->
