



<?php 
	if(get_field('pamphlets_download')){
		$pamphlets_posts = get_field('pamphlets_download');
	}
	foreach($pamphlets_posts as $p){
	//vars
	$post_id = $p->ID;
	$post_title = $p->post_title;
	$post_subtitle = get_field('subtitle', $post_id);
	$content = $p->post_content;
	$file_id = get_field('pamphlet', $post_id);
	$file_url = wp_get_attachment_url( $file_id );
	$bytes = filesize( get_attached_file( $file_id ) );
	if ($bytes >= 1073741824) $bytes = number_format($bytes / 1073741824, 2). ' GB';
    elseif ($bytes >= 1048576) $bytes = number_format($bytes / 1048576, 2). ' MB';
    elseif ($bytes >= 1024) $bytes = number_format($bytes / 1024, 2). ' KB';
    elseif ($bytes > 1) $bytes = $bytes. ' bytes';
    elseif ($bytes == 1) $bytes = $bytes. ' byte';
    else $bytes = '0 bytes';
    
    // PDFの0ページ目を画像として取得
    $file_thumbnail = new imagick($file_url[0]);
    // PNG 形式に変換
    $file_thumbnail -> setImageFormat("png");
    // 長辺が 300 ピクセルになるようにリサイズ
	$file_thumbnail -> thumbnailImage(300, 300, true);
    // 表示
	header("Content-Type: image/png");
	//echo
	echo '<div class="entryCont">';
	if ($file_thumbnail) {
	echo '<p>'. $post_title .'</p>';
	echo '<div class="cf">';
	echo '<div>';
	if($content){echo wp_kses_post($content);}
	echo '</div>';
		echo '<div class="dl_pdf">';
		echo $file_thumbnail;
		echo '<a class="pdf-thumbnail-link over" href="'.$file_url.'" title="'.esc_html( $post_title ).'" target="_blank"><img src="'.get_template_directory_uri().'/common/images/common/img_dl.png" alt="ダウンロード"/><img src="'.get_template_directory_uri().'/common/images/common/pdf_sp.png" alt="PDFダウンロード" width="100%" class="spPdf"></a>';
	
	echo '<p class="size">'.$bytes.'</p>';
	echo '</div>';
	
	echo '</div>';
	echo '</div>';
	}
}

