<?php 
			// フレキシブルコンテンツ
			if( have_rows('flexible_content') ) {
				$cnt_proposal_why = 0;
				while ( have_rows('flexible_content') ) {
					the_row();
					
					// 見出し（大）
			        if( get_row_layout() == 'header_l' ) {
								$cnt_proposal_why++;
			        	echo '<h1 class="page-title" id="why0'.$cnt_proposal_why.'">'.esc_html(get_sub_field('header_l_title')).'</h1>';
			        }
			        
			        // 見出し（中）
			        elseif( get_row_layout() == 'header_m' ) {
			        	echo '<h2>'.esc_html(get_sub_field('header_m_title')).'</h2>';
			        }
			        
			        // 見出し（小）
			        elseif( get_row_layout() == 'header_s' ) {
			        	echo '<h3>'.esc_html(get_sub_field('header_s_title')).'</h3>';
			        }
			        
			        // 自由記入欄
			        elseif( get_row_layout() == 'wysiwyg' ) {
			        	echo '<div class="wysiwyg_content clear">'.wp_kses_post(get_sub_field('wysiwyg_content')).'</div>';
			        }
			        
			        // 2カラム（自由記入欄）
			        elseif( get_row_layout() == 'two-column' ) {
				        echo '<div class="two-column-container">';
				        if( get_sub_field('left-column') ){
					        echo '<div class="column">';
					        echo wp_kses_post(get_sub_field('left-column'));
					        echo '</div>';
					    }
				        if( get_sub_field('right-column') ){
					        echo '<div class="column">';
					        echo wp_kses_post(get_sub_field('right-column'));
					        echo '</div>';
					    }
				        echo '</div>';
			        }
			        
					// 表
					elseif( get_row_layout() == 'table' ) {
			        	$col = get_sub_field('col');
								if(get_sub_field('no_header') == '使う'){
									$no_header = 1;
								} else {
									$no_header = 0;
								}
			        	if( have_rows('row') ) {
				        	echo '<table class="table3 sp-hide_table">';
									$cnt_tr = 0;
				        	while (have_rows('row')): the_row();
										$cnt_tr++;
				        		echo '<tr>';
				        			for($i=1;$i<=$col;$i++){
												if($no_header === 1){
													$cell_content = '<th>'.get_sub_field('col'.$i).'</th>';
													if ($cnt_tr == 1){
													} else {
													$cell_content = '<td>'.get_sub_field('col'.$i).'</td>';
													}
												} else {
													$cell_content = '<td>'.get_sub_field('col'.$i).'</td>';
												}
					        			echo wp_kses_post($cell_content);
				        			}
				        		echo '</tr>';
				        	endwhile;
				        	echo '</table>';
			        	}
					}

					// 流れの説明
			        elseif( get_row_layout() == 'flow' ) {
								echo '<ul class="numCont">';								
								
				        if( have_rows('flow_set') ) {
			        		$flow_cnt = 1;
			        		while (have_rows('flow_set')) {
										the_row();
										$flow_cnt++;
									}
			        		$i = 1;
			        		while (have_rows('flow_set')) {
				        		the_row();
										if(post_custom('insert_header')){
										echo '<h2>'.get_sub_field('insert_header').'</h2>';
										}
										echo '<li class="cf">';
										echo '<header class="cf">';
										echo '<p class="sub">'.esc_html(get_sub_field('flow_set_subtitle')).'</p>';
										echo '<dl class="cf"><dt><p>'.$i.'</p></dt>';
										$i++;
										echo '<dd><h4>'.esc_html(get_sub_field('flow_set_title')).'</h4></dd></dl>';
										echo '</header>';
										if(get_sub_field('flow_set_thumbnail')){
					        		$thumb_src = wp_get_attachment_image_src ( get_sub_field('flow_set_thumbnail'), 'medium' );
					        		echo '<img src="'. $thumb_src[0] .'" class="thumb"/>';
										}
										echo '<div>'.wp_kses_post(get_sub_field('flow_set_content')).'</div>';
										echo '<div class="clear"></div>';
										echo '</li>';
			        		}
				        }
								echo '</ul>';
			        }

					// 実績紹介
			        elseif( get_row_layout() == 'works' ) {
			        	get_template_part('works');
			        }
					
			        // 写真とキャプションと本文のセット
			        elseif( get_row_layout() == 'photo_caption_content_set' ) {
						if( get_sub_field('set_photo') ){
							$img = get_sub_field('set_photo');
							$imgurl = wp_get_attachment_image_src($img, 'medium');
							echo '<figure class="wp-caption alignright"><img src="'.$imgurl[0].'" alt="" />';
							//写真キャプション
							echo '<figcaption class="wp-caption-text">';
							if( get_sub_field('set_photocaption') ){
								echo nl2br(get_sub_field('set_photocaption'));
							}
							echo '</figurecaption>';
							echo '</figure>';
						}
						if( get_sub_field('set_content') ){
							echo '<div class="wysiwyg_content cf">'.wp_kses_post(get_sub_field('set_content')).'</div>';
						}	
			        }

					// 質問と回答
			        elseif( get_row_layout() == 'faq_qa' ) {
				        if( have_rows('faq_qa_set') ) {
					        while (have_rows('faq_qa_set')) {
						        the_row();
				        		echo '<div class="qa"><dl id="acMenu">';
				        		echo '<dt><img src="'.get_template_directory_uri().'/common/images/common/icon_q.png" alt="q">'.wp_kses_post(get_sub_field('faq_qa_set_question')).'<div class="arrow"><img src="'.get_template_directory_uri().'/common/images/common/btn_bottom.png" width="100%" alt=""></div></dt>';
				        		echo '<dd><img src="'.get_template_directory_uri().'/common/images/common/icon_a.png" alt="a">'.wp_kses_post(get_sub_field('faq_qa_set_answer')).'</dd>';
				        		echo '</dl></div>';
					        }
				        }
			        }
			        
			        // お知らせの表示
			        elseif( get_row_layout() == 'show_posts' ){
				        get_template_part('news');
			        }
			        
			        // お問い合わせ・資料請求
			        elseif( get_row_layout() == 'inq' ) {
				        echo '<div class="inq cf">'."\n";
				        echo '<h3>お問い合わせ・資料請求</h3>'."\n";
				        if( get_sub_field('inq_wysiwyg') ){
					        echo wp_kses_post(get_sub_field('inq_wysiwyg'))."\n";
				        }
				        if( have_rows('tel_repeater') ){
					        echo '<div class="cf">'."\n".'<div class="tel">'."\n".'<h4>お電話でのお問い合わせはこちら</h4>'."\n";
					        while (have_rows('tel_repeater')) {
						        the_row();
						        if(substr(get_sub_field('tel'), 0, 4) == '0120'){
							        echo '<p class="freedial">';
						        } else {
							        echo '<p>';
						        }
						        if(get_sub_field('department')){
							        echo '<span>['.esc_html(get_sub_field('department')).']</span>';
						        }
						        echo esc_html(get_sub_field('tel')).'</p>'."\n";
					        }
					        echo '</div>'."\n".'</div>'."\n";
				        }
				        if( get_sub_field('inq_form_url') ){
					        echo '<div class="website">'."\n".'<h4>WEBサイトからのお問い合わせはこちら</h4>'."\n";
					        echo '<a style="opacity: 1;" href="';
					        echo esc_url(get_sub_field('inq_form_url'));
					        echo '" class="over">› お問い合わせフォーム</a>'."\n";
					        echo '</div>'."\n";
				        }
				        echo '</div>'."\n";
			        }
			        
			        // 資料ダウンロード
			         elseif( get_row_layout() == 'pamphlets_download_layout'){
						echo '<h2>資料ダウンロード</h2>';
						get_template_part('pamphlets');
			         }
					
				} // end while ( have_rows('flexible_content') )
			} // end if( have_rows('flexible_content') )
