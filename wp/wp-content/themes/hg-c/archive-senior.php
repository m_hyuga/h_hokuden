<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        <div class="mainImg_bg">
		<p>採用情報</p><div class="mainImg"><img src="<?php echo get_template_directory_uri(); ?>/common/images/common/midashi/ttl_saiyou.jpg" width="100%" alt="採用情報"></div>
        </div>
		<?php	get_template_part( 'content-pan' );?>
		<div class="content column2">
		<?php get_sidebar(); ?>
		<div class="rightCont"><!-- 右コンテンツここから -->
		<?php if ( have_posts() ) : ?>

			<header class="page-header">
        
            
				<h1 class="page-title">先輩が贈るひとこと</h1>
				<?php
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();
			?>
				<div id="post-<?php the_ID(); ?>" class="<?php if ( has_post_thumbnail() ) {echo 'achList';}else{echo 'index-article';}; if( get_field('index_link') != 1 ){ echo ' no_right';} ?> cf">
				<div class="fll">
                	<?php
		// サムネイル
		if ( has_post_thumbnail() ) {
			echo '<p class="img_left"><a href="'.get_the_permalink().'">';
			the_post_thumbnail();
			echo '</a></p>';
		}
		
		// カスタムフィールド
		$updated_date       = get_field('updated_date');
		$affiliation        = get_field('affiliation');
		$graduate           = get_field('graduate');
		$my_job_title       = get_field('my_job_title');
		$my_job             = get_field('my_job');
		$n_opt = mb_strimwidth( $n_trm, 0, 150, "...", "UTF-8" );

		// 所属
		if ( $affiliation ) {
			echo '<p class="affiliation_title">' . esc_html( $affiliation ) . '</p>';
		}

		if ( is_single() ) :
			// カスタムフィールド
			$episode_title      = get_field('episode_title');
			$episode            = get_field('episode');
			$reason_title       = get_field('reason_title');
			$reason             = get_field('reason');
			$career             = get_field('career');
			$job_cagtegory      = get_field('job_category');
			$description        = get_field('description');
			$style              = get_field('style');
			$person             = get_field('person');
			$acquisition         = get_field('acquisition');
			$personality        = get_field('personality');
			$advice             = get_field('advice');

			// 名前
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
		endif;

		// 出身
		if ( $graduate ) {
			echo '<p class="graduate_title"><img src="'. get_template_directory_uri().'/common/images/recruit/titile_graduate.jpg" alt="出身" width＝”5%”>'. esc_html( $graduate ) . '</p>';
		}
		
		// 出身
		if ( $my_job_title ) {
			echo '<h2 class="job_info"><a href="'.get_the_permalink().'">これが私の仕事</a></h2>';
		}
		
		
						//業務分類
						$classification = get_the_terms( $post -> post_id, 'classification' );
						if($classification){
							$count = 0;
							foreach($classification as $c){
								echo '<span class="classification id_'.esc_html($classification[$count] -> parent).'">'.esc_html($classification[$count] -> name).'</span>';
								$count++;
								if ($c != end($classification)) { echo '&nbsp;';}
							}
						}
					
					
						//本文抜粋
						
						echo '<h3 class="job_title">' . get_field('my_job_title') . '</h3>';
					
						$text = mb_substr(get_field('my_job'),0,80,'utf-8'); 
						echo $text.'...','<a href="'.get_the_permalink().'">続きをみる</a>';
						
						
						
						
						
						$theContentForPreSingle = mb_substr(strip_tags($post-> post_content), 0, 100);
						$theContentForPreSingle = preg_replace('/\［caption(.+?)\/caption\］/','',$theContentForPreSingle);
						$theContentForPreSingle = preg_replace('/\［gallery(.+?)\］/','',$theContentForPreSingle);
						echo '<p>';
						echo $theContentForPreSingle;
						if(mb_strlen($theContentForPreSingle) >= 100){echo '...';};
						echo '</p>';
						
						echo '<div>';
						//年度
						$fiscal_year = get_the_terms( $post -> post_id, 'fiscal_year' );
						if($fiscal_year){
							$count = 0;
							foreach($fiscal_year as $f){
								echo '<span class="fiscal_year">'.esc_html($fiscal_year[$count] -> name).'</span>';
								$count++;
							}
						}
						//発注者名
						$client = get_the_terms( $post -> post_id, 'client' );
						//年度・発注者両方ある時' / '
						if(!empty($fiscal_year) and !empty($client)){
							echo ' / ';
						}
						if($client){
							echo '<span class="client">'.esc_html($client[0] -> name).'</span>';
						}
						echo '</div></div>';
						
						//詳しくはこちら
						if( get_field('index_link') == 1 ){
							if ( has_post_thumbnail() ) {
								echo '<div class="btn_deta2">';
								$image_id = get_post_thumbnail_id();
								$image_url = wp_get_attachment_image_src($image_id, true);
								?><p style="background-image:url('<?php
								echo  $image_url[0];
								?>')"></p><?php
							}
							else{
								echo '<div class="btn_deta">';
							}
							echo '<a href="';
							the_permalink();
							echo '" class="over"><span>&#155;</span>詳しくはこちら</a></a></div>';
						}

						//アイキャッチ
						
					?>
				</div>
			<?php
			// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( '', 'twentyfifteen' ),
				'next_text'          => __( '', 'twentyfifteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'twentyfifteen' ) . ' </span>',
				'mid_size' => 3,
			) );

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>
		</div><!-- /右コンテンツここまで -->
		</div>
		</main><!-- .site-main -->
	</section><!-- .content-area -->
<?php get_footer(); ?>
