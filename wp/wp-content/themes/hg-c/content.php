<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
 if ( is_single() ) :
?>
<script type='text/javascript' src='http://www.hg-c.co.jp/wp/wp-content/themes/hg-c/common/js/jquery.tile.js'></script>
<script>
	jQuery(window).on('load resize', function() {
		jQuery("figure[id ^= 'attachment_'] figcaption").tile(2);
	});
</script>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		// Post thumbnail.
		//the_post_thumbnail('full');
	?>
	<header class="entry-header">
		<?php	the_title( '<h1 class="entry-title">', '</h1>' );?>
		<p>
		<?php the_time('Y年m月d日'); ?>　
		<?php $cats = get_the_terms( $post->ID, 'category' );
											foreach( $cats as $c ){
												echo '<span class="icon'.$c->name.'" class="over"><a href="'.get_category_link($c->term_id).'">'.$c->name.'</a></span>';
											}
								 ?>
		</p>
	</header><!-- .entry-header -->
		<div class="entry-summary">
			<?php the_content(); ?>
		</div><!-- .entry-summary -->
		<?php
			/* translators: %s: Name of current post */
			/*the_content( sprintf(
				__( 'Continue reading %s', 'twentyfifteen' ),
				the_title( '<span class="screen-reader-text">', '</span>', false )
			) );*/

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfifteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		?>


	<footer class="entry-footer">
		<?php //twentyfifteen_entry_meta(); ?>
		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->


<?php
	else :
?>
<li>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		// Post thumbnail.
		//the_post_thumbnail('full');
	?>
	<header class="entry-header">
	<br>
		<?php the_title( sprintf( '<h2><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</header><!-- .entry-header -->
		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
		<?php
			/* translators: %s: Name of current post */
			/*the_content( sprintf(
				__( 'Continue reading %s', 'twentyfifteen' ),
				the_title( '<span class="screen-reader-text">', '</span>', false )
			) );*/

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfifteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		?>

	<footer class="entry-footer">
		<?php //twentyfifteen_entry_meta(); ?>
		<p>
		<?php the_time('Y年m月d日'); ?>　
		<?php $cats = get_the_terms( $post->ID, 'category' );
											foreach( $cats as $c ){
												echo '<span class="icon'.$c->name.'" class="over"><a href="'.get_category_link($c->term_id).'">'.$c->name.'</a></span>';
											}
								 ?>
		</p>
		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
</li>
<?php
	endif;
?>