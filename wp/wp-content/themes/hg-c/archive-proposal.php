<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<script>
jQuery(window).on('load resize', function() {
	jQuery('.centerCont li').tile(3);
});
</script>
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        <div class="mainImg_bg">
		<p>わたしたちの提案</p><div class="mainImg"><img src="<?php echo get_template_directory_uri(); ?>/common/images/common/midashi/tte_teian.jpg" width="100%" alt=""></div>
        </div>
		<?php	get_template_part( 'content-pan' );?>
		<div class="content column1">
		<div class="centerCont">

			<header class="page-header">
				<!--<h1 class="page-title">実績紹介</h1>-->
				<?php
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
				<h1 class="page-title"><?php echo esc_html(get_post_type_object(get_post_type())->label ); ?>一覧</h1>
				<nav>
				<ul>
					<?php
					$proposallist = array(7, 15, 17);
					foreach ($proposallist as $proposal){
						$posts = get_posts(array("include"=>$proposal, "post_type"=>'proposal')); global $post; 
						if($posts): foreach($posts as $post): setup_postdata($post); $id = $post->ID;
							echo '<li>';
							echo '<a href="'.get_the_permalink().'"><p class="thumb over" style="background-image:url(\'';
							//画像(返り値は「画像ID」)
										$attachment_id = get_field('lead_images01', $id);
										$size = "full"; // (thumbnail, medium, large, full or custom size)
										$image = wp_get_attachment_image_src( $attachment_id, $size );
										echo $image[0];
							echo '\')"></p></a>';
							echo '<h4><a href="'.get_the_permalink().'">'.get_the_title().'</a></h4>';
							echo '<div>'.esc_html(get_field('lead_sentence', $id)).'</div>';
							echo '<div class="detaBtn"><a href="'.get_the_permalink().'" class="over"><img src="'.get_template_directory_uri().'/common/images/common/arrow_gray.png" width="100%" alt="">詳細をみる</a></div>';
							echo '</li>';
						endforeach; endif; wp_reset_postdata();
						}
					 ?>
				</ul>
				</nav>
			</header><!-- .page-header -->

		</div><!-- /右コンテンツここまで -->
		</div>
		</main><!-- .site-main -->
	</section><!-- .content-area -->
<?php get_footer(); ?>
