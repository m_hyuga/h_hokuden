<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/common/images/common/favicon.ico" />
	<style>
		/* 2カラム自由記入欄 */
		.two-column-container {
			overflow: hidden;
			margin-bottom: 40px;
		}
		.two-column-container .column  {
			float: left;
			width: 50%;
			padding-right: 12px;
		}
		.two-column-container .column:last-child {
			padding-right: 0;
		}
		.two-column-container .column img {
			width: 100%;
			height: auto;
		}

	</style>
</head>

<body <?php if(is_front_page()){ echo 'id="top"'; }; body_class('main_site'); ?>>
<div id="header" class="cf">
	<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="over"><img src="<?php echo get_template_directory_uri(); ?>/common/images/common/img_h1.jpg" width="100%" alt="北電技術コンサルタント株式会社"></a></h1>
	<!-- SPメニュ ここから -->
	<div id="spBtn"></div>
	<div id="spMenu">
		<div class="spSearch">
				<?php
					// SP検索フォーム
				echo '<form role="search" method="get" id="searchform" action="'.home_url( '/' ).'" >
				<label class="screen-reader-text" for="s"></label>
				<input type="text" value="' . get_search_query() . '" placeholder="サイト内検索" name="s" id="s" class="keywords">
				<button class="searchBtn"><img src="'.  get_template_directory_uri() .'/common/images/common/img_search_sp.png" class="imgSearch" alt=""></button>
				</form>'; ?>
		
		</div>
		<ul class="spMenu_inner">
			<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>business/">事業案内</a></li>
			<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>works/">実績紹介</a></li>
			<li class="cf"><a href="<?php echo esc_url( home_url( '/' ) ); ?>proposal/">わたしたちの提案</a><img src="<?php echo get_template_directory_uri(); ?>/common/images/common/sp_innerBtn.jpg" class="spInnerBtn" width="100%" alt=""></li>
			<li id="subInnerMenu">
				<ul>
					<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>proposal/proposal-top/small-hydro/">小水力発電</a></li>
					<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>proposal/proposal-top/welfare-facilities/">介護福祉施設</a></li>
					<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>proposal/proposal-top/structural-design-p/">構造設計</a></li>
				</ul>
			</li>
			<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>quality/our-quolity/">品質への取り組み</a></li>
			<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>arch/hgc-architects/">HGC建築設計事務所</a></li>
			<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>recruit/recruit-top/">採用情報</a></li>
			<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>company/company-top/">会社案内</a></li>
			<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>contact/">お問い合わせ</a></li>
		</ul>
		<div class="spInq cf">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>news/">お知らせ</a>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>sitemap/">サイトマップ</a>
			<a class="privacy" href="<?php echo esc_url( home_url( '/' ) ); ?>privacy/">個人情報保護方針</a>
		</div>
	</div>
	<!-- /SPメニュ ここまで -->
	<div id="nav">
		<div class="nav_top cf">

			<ul class="cf">
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>news/" class="over">お知らせ</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>sitemap/" class="over">サイトマップ</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>privacy/" class="over">個人情報保護方針</a></li>
			</ul>
			<div class="logo"><img src="<?php echo get_template_directory_uri(); ?>/common/images/common/img_logo.jpg" width="100%" alt="北陸電力グループ"></div>
		</div>
		<div class="headBtnNav cf">
			<div class="search">
				<?php
					// PC検索フォーム
				echo '<form role="search" method="get" id="searchform" action="'.home_url( '/' ).'" >
				<label class="screen-reader-text" for="s"></label>
				<input type="text" value="' . get_search_query() . '" placeholder="サイト内検索" name="s" id="s" class="keywords">
				<button class="searchBtn"><img src="'.  get_template_directory_uri() .'/common/images/common/img_search.png" width="100%" alt=""></button>
				</form>'; ?>
			</div><!-- /search -->
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>contact/" class="btn_inq over"><span>&rsaquo;</span>お問い合わせ</a>
		</div>
	</div><!-- /nav -->
</div><!-- /header -->
<div class="bg_menu">
<div class="menu_f">
	<ul class="menu cf">
	<?php $post_type = get_post_type(); ?>
		<li<?php if (is_singular('business') || $post_type == 'business'){ echo ' class="page_now"';}?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>business/">
			事業案内<span>business</span>
		</a></li>
		<li<?php if (is_singular('works') || $post_type == 'works'){ echo ' class="page_now"';}?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>works/">
			実績紹介<span>works</span>
		</a></li>
		<li class="pro pulldownMenu<?php if (is_singular('proposal') || $post_type == 'proposal'){ echo ' page_now';}?>"><a href="<?php echo esc_url( home_url( '/' ) ); ?>proposal/">
			わたしたちの提案<span>proposition</span>
		</a>
			<ul class="innerMenu">
				<li<?php if (is_single('small-hydro')){ echo ' class="page_now"';}?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>proposal/proposal-top/small-hydro/">小水力発電</a></li>
				<li<?php if (is_single('welfare-facilities')){ echo ' class="page_now"';}?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>proposal/proposal-top/welfare-facilities/">介護福祉施設</a></li>
				<li<?php if (is_single('structural-design-p')){ echo ' class="page_now"';}?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>proposal/proposal-top/structural-design-p/">構造設計</a></li>
			</ul></li>
		<li class="qua<?php if (is_singular('quality')){ echo ' page_now';}?>"><a href="<?php echo esc_url( home_url( '/' ) ); ?>quality/our-quolity/">
			品質への取り組み<span>quality</span>
		</a></li>
		<li class="hcg<?php if (is_singular('arch') || $post_type == 'arch' || is_singular('specialists') || $post_type == 'specialists'){ echo ' page_now';}?>"><a href="<?php echo esc_url( home_url( '/' ) ); ?>arch/hgc-architects/">
			HGC建築設計事務所<span>HGC architectural</span>
		</a></li>
		<li<?php if (is_singular('recruit') || $post_type == 'recruit' || is_singular('senior') || $post_type == 'senior'){ echo ' class="page_now"';}?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>recruit/recruit-top/">
			採用情報<span>recruit</span>
		</a></li>
		<li<?php if (is_singular('company') || $post_type == 'company'){ echo ' class="page_now"';}?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>company/company-top/">
			会社案内<span>company</span>
		</a></li>
	</ul>
    </div>
</div><!-- /bg_menu -->

