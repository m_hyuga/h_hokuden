<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php
			if ( is_single() ) :
				the_title( '<h1 class="page-title">', '</h1>' );
			else :
				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
			endif;
		?>
	</header><!-- .entry-header -->
	<?php
		// Post thumbnail.
		twentyfifteen_post_thumbnail();
	?>

	<div class="entry-content">
		<?php
			//業務内容（サービス内容）
			if($post->post_content!=""){
				echo '<h2>業務内容（サービス内容）</h2>';
				echo '<div class="wysiwyg_content">';
				the_content();
				echo '</div>';
			}
			//業務内容の詳細
			if( have_rows('detail') ){
				while( have_rows('detail') ){
					the_row();
					//vars
					$title = get_sub_field('title');
					echo '<div class="wysiwyg_content">';
					$content = get_sub_field('content');
					echo '</div>';
					echo '<div class="clear">';
					if($title){echo '<h3>'.esc_html($title).'</h3>';}
					if($content){echo '<div class="wysiwyg_content">'.wp_kses_post($content).'</div>';}
					echo '</div>';
				}
			}
			//技術紹介
			if( have_rows('technology') ){
				echo '<h2>技術紹介</h2>';
				while( have_rows('technology') ){
					the_row();
					//vars
					$title = get_sub_field('title');
					echo '<div class="wysiwyg_content">';
					$content = get_sub_field('content');
					echo '</div>';
					echo '<div class="clear">';
					if($title){echo '<h3>'.esc_html($title).'</h3>';}
					if($content){echo '<div class="wysiwyg_content">'.wp_kses_post($content).'</div>';}
					echo '</div>';
				}
			}
			//実績紹介
			$works_posts = get_field('works');
			if($works_posts){
				echo '<h2>実績紹介</h2>';
			    get_template_part('works');
			}
			//担当者から
			if( have_rows('person') ){
				echo '<h2>担当者から</h2>';
				while( have_rows('person') ){
					the_row();
					//vars
					$title = get_sub_field('title');
					$name = get_sub_field('name');
					$content = get_sub_field('content');
					echo '<div class="tantou clear">';
					if($content){echo '<div>'.wp_kses_post($content).'</div>';}
					if($title or $name){
						echo '<h4 class="resname">';
						if($title){echo '<span>'.esc_html($title).'</span>';}
						if($name){echo esc_html($name);}
						echo '</h4>';
					}
					echo '</div>';
				}
			}
			//このページに表示するパンフレット
			$pamphlets_posts = get_field('pamphlets_download');
			if($pamphlets_posts){
				echo '<h2>資料ダウンロード</h2>';
				get_template_part('pamphlets');
			}
		?>
	</div><!-- .entry-content -->

	<?php
		// Author bio.
		if ( is_single() && get_the_author_meta( 'description' ) ) :
			get_template_part( 'author-bio' );
		endif;
	?>

	<footer class="entry-footer">
		<?php twentyfifteen_entry_meta(); ?>
		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
