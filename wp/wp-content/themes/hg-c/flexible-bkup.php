<?php 
			// check if the flexible content field has rows of data
			if( have_rows('flexible_content') ):
			
			     // loop through the rows of data
			    while ( have_rows('flexible_content') ) : the_row();
					//見出し（大）
			        if( get_row_layout() == 'header_l' ):
			        	echo '<h2>'.esc_html(get_sub_field('header_l_title')).'</h2>';
					//見出し（中）
			        elseif( get_row_layout() == 'header_m' ): 
			        	echo '<h3>'.esc_html(get_sub_field('header_m_title')).'</h3>';
					//見出し（小）
			        elseif( get_row_layout() == 'header_s' ): 
			        	echo '<h4>'.esc_html(get_sub_field('header_s_title')).'</h4>';
					//自由記入欄
			        elseif( get_row_layout() == 'wysiwyg' ): 
			        	echo '<div class="clear">'.wp_kses_post(get_sub_field('wysiwyg_content')).'</div>';
					//表
			        elseif( get_row_layout() == 'table' ): 
			        		$col = get_sub_field('col');
			        	if( have_rows('row') ) :
				        	echo '<table>';
				        	while (have_rows('row')): the_row();
				        		echo '<tr>';
				        			for($i=1;$i<=$col;$i++){
					        			$cell_content = get_sub_field('col'.$i);
					        			echo '<td>'.wp_kses_post($cell_content).'</td>';
				        			}
				        		echo '</tr>';
				        	endwhile;
				        	echo '</table>';
			        	endif;
			
					//流れの説明
			        elseif( get_row_layout() == 'flow' ): 
			        	if( have_rows('flow_set') ) :
			        		$i = 1;
				        	while (have_rows('flow_set')): the_row();
				        		if(get_sub_field('insert_header')){
					        		echo '<h2>'.get_sub_field('insert_header').'</h2>';
				        		}
				        		echo '<div class="flow_set">';
				        		echo '<p class="flow_count">'.$i.'</p>';
				        		echo '<div class="flow_content_unit">';
				        		if(get_sub_field('flow_set_subtitle')){
				        			echo '<h6 class="flow_set_subtitle">'.esc_html(get_sub_field('flow_set_subtitle')).'</h6>';
				        		}
				        		echo '<h5 class="flow_set_title">'.esc_html(get_sub_field('flow_set_title')).'</h5>';
				        		if(get_sub_field('flow_set_content')){
				        			echo '<div>'.wp_kses_post(get_sub_field('flow_set_content')).'</div>';
				        		}
				        		if(get_sub_field('flow_set_thumbnail')){
					        		$thumb_src = wp_get_attachment_image_src ( get_sub_field('flow_set_thumbnail'), 'medium' );
					        		echo '<img src="'. $thumb_src[0] .'" width="'. $thumb_src[1] .'" height="'. $thumb_src[2] .'"/>';
				        		}
				        		echo '</div>';
				        		echo '</div>';
				        		$i++;
				        	endwhile;
			        	endif;
					//実績紹介
			        elseif( get_row_layout() == 'works' ):
			        get_template_part('works');
			        //写真とキャプションと本文のセット
			        elseif( get_row_layout() == 'photo_caption_content_set' ): 
						if( get_sub_field('set_photo') ){
							$img = get_sub_field('set_photo');
							$imgurl = wp_get_attachment_image_src($img, 'medium');
							echo '<figure class="wp-caption alignright"><img src="'.$imgurl[0].'" alt="" />';
							//写真キャプション
							echo '<figcaption class="wp-caption-text">';
							if( get_sub_field('set_photocaption') ){
								echo nl2br(get_sub_field('set_photocaption'));
							}
							echo '</figurecaption>';
							echo '</figure>';
						}
						
						//本文
						if( get_sub_field('set_content') ){
							echo '<div>'.wp_kses_post(get_sub_field('set_content')).'</div>';
						}
			        
			        endif;
			    endwhile;
			
			endif;
