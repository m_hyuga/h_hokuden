<div class="mainImg_bg">
<?php
	if (is_singular('business')||is_post_type_archive('business')){//事業案内
		echo '<p>事業案内</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/title/ttl_jigyou.jpg" width="100%" alt="事業案内"></div>';
	}
	if (is_singular('works')||is_post_type_archive('works')){//実績紹介
		echo '<p>実績紹介</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/title/ttl_jisseki.jpg" width="100%" alt="実績紹介"></div>';
	}
	if (is_post_type_archive('proposal')){//わたしたちの提案
		echo '<p>わたしたちの提案</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/title/tte_teian.jpg" width="100%" alt=""></div>';
	}
	if (is_single('small-hydro') || is_parent_slug() === 'small-hydro'){ //小水力発電
		echo '<p><span>わたしたちの提案</span>小水力発電</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/title/ttl_chousuiryoku.jpg" width="100%" alt="小水力発電"></div>';
	}
	if (is_single('welfare-facilities') || is_parent_slug() === 'welfare-facilities'){ //介護福祉施設
		echo '<p><span>わたしたちの提案</span>介護福祉施設</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/title/ttl_kaigo.jpg" width="100%" alt="介護福祉施設"></div>';
	}
	if (is_single('structural-design-p')){ //構造設計
		echo '<p><span>わたしたちの提案</span>構造設計</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/title/ttl_kouzou.jpg" width="100%" alt="構造設計"></div>';
	}
	if (is_singular('quality')){
		echo '<p>品質への取り組み</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/title/ttl_hinshitsu.jpg" width="100%" alt="品質への取り組み"></div>';
	}
	if (is_singular('recruit')||is_post_type_archive('recruit') ||is_post_type_archive('senior') || is_singular('senior')){
		echo '<p>採用情報</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/title/ttl_saiyou.jpg" width="100%" alt="採用情報"></div>';
	}
	if (is_singular('company')){
		echo '<p>会社案内</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/title/ttl_conpany.jpg" width="100%" alt="会社案内"></div>';
	}
	if (is_home() ||  is_category() || is_singular('post')){
		echo '<p>お知らせ</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/title/ttl_news.jpg" width="100%" alt="お知らせ"></div>';
	}

		if(is_page('sitemap')){
			echo '<p>サイトマップ</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/title/ttl_saitemap.jpg" width="100%" alt=""></div>';
		};
		if(is_page('contact') || is_parent_slug() === 'contact'){
			echo '<p>お問い合わせ</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/title/ttl_form.jpg" width="100%" alt=""></div>';
		};
		if(is_page('privacy')){ 
			echo '<p>個人情報保護方針</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/title/ttl_privacy.jpg" width="100%" alt=""></div>';
		};
?>
</div>
