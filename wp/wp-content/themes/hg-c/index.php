<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main oshirase" role="main">

			<?php if ( is_home() or  is_category() or ! is_front_page() ) : ?>
		<?php
		get_template_part( 'content-img' );
		get_template_part( 'content-pan' );
		?>
		<div class="content column2">
		<?php if ( have_posts() ) : ?>
		<?php get_sidebar(); ?>
			<?php endif; ?>
			<div class="rightCont"><!-- 右コンテンツここから -->
			<div class="entry-content">
			<?php
			if ( is_category ()){
				echo '<h1 class="page-title">お知らせ：';
				single_cat_title();
				echo '</h1>';
			}
			if ( is_year ()){
				echo '<h1 class="page-title">お知らせ：';
				the_time('Y年');
				echo '</h1>';
			}
			?>
			<ul>
			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'content', get_post_format() );

			// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( '', 'twentyfifteen' ),
				'next_text'          => __( '', 'twentyfifteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'twentyfifteen' ) . ' </span>',
				'mid_size' => 3,
			) );

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>
		</ul>
		</div>
		</div>
		</div>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
