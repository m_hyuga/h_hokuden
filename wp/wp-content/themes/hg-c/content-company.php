<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			//スラッグ
			$post_slug = attribute_escape( $post->post_name );
			
			/* =========
			 会社案内トップ
			===== */
			if($post_slug == 'company-top'){
				//お知らせ
				get_template_part('news');

				//子ページのリンク
				$child_posts = get_posts( 'numberposts=-1&order=ASC&orderby=menu_order&post_type=company&post_parent=' . $post->ID );
				echo '<ul class="links cf">';
				if ( $child_posts ) {
					$loopNumber = 1;
					foreach ( $child_posts as $c ) {
						$str = '<li class="listn'.$loopNumber.'"><a href="';
						$str .= $c->guid;
						$str .= '">';
						$str .= $c->post_title;
						$str .= '</a></li>';
						echo $str;
						$loopNumber ++ ;
					} //end foreach
				} //end if
				echo '</ul>';
			}
			
			/* =========
			 社長挨拶・経営方針
			===== */
			elseif($post_slug == 'greeting'){
				//社長挨拶
				//サブタイトル
				if( get_field('set_subtitle') ){
					echo '<h2>'.esc_html(get_field('set_subtitle')).'</h2>';
				}
				echo '<div class="company_greeting cf">';
				//本文
				if( get_field('set_content') ){
					echo '<div class="wysiwyg_content">'.wp_kses_post(get_field('set_content')).'</div>';
				}
				//写真
				if( get_field('set_photo') ){
					$img = get_field('set_photo');
					$imgurl = wp_get_attachment_image_src($img, 'medium');
					echo '<figure class="wp-caption alignright"><img src="'.$imgurl[0].'" alt="" />';
					//写真キャプション
					echo '<figcaption class="wp-caption-text">';
					if( get_field('set_photocaption') ){
						echo nl2br(get_field('set_photocaption'));
					}
					echo '</figurecaption>';
					echo '</figure>';
				}
					echo '</div>';
				//経営方針タイトル
				if( get_field('vision_title') ){
					echo '<h2>'.esc_html(get_field('vision_title')).'</h2>';
				}
				//経営方針の内容
				if( have_rows('vision_content') ){
					while( have_rows('vision_content') ){
						the_row();
						//vars
						$subtitle = get_sub_field('vision_subtitle');
						$subcontent = get_sub_field('vision_subcontent');
						echo '<div class="clear policy">';
						if($subtitle){echo '<h4 class="tit">'.esc_html($subtitle).'</h4>';}
						if($subcontent){echo '<div>'.wp_kses_post($subcontent).'</div>';}
						echo '</div>';
					} 
				}
			}

			/* =========
			 会社概要・沿革
			===== */
			elseif($post_slug == 'overview') {
				//会社概要タイトル
				if( get_field('overview_title') ){
					echo '<h2>'.esc_html(get_field('overview_title')).'</h2>';
				}
				//会社概要の表
				if( have_rows('overview_table') ){
					$str = '<table class="table1">';
					while( have_rows('overview_table') ){
						the_row();
						$str .= '<tr>';
						//vars
						$table_header = get_sub_field('overview_table_header');
						$table_content = get_sub_field('overview_table_content');
						if($table_header){$str .= '<th class="label">'.esc_html($table_header).'</th>';}
						if($table_content){$str .= '<td>'.wp_kses_post($table_content).'</td>';}
						$str .= '</tr>';
					} 
					$str .= '</table>';
					echo $str;
				}
				//沿革タイトル
				if( get_field('history_title') ){
					echo '<h2>'.esc_html(get_field('history_title')).'</h2>';
				}
				//沿革の表
				if( have_rows('history_table') ){
					$str = '<table class="table1">';
					while( have_rows('history_table') ){
						the_row();
						$str .= '<tr>';
						//vars
						$table_header = get_sub_field('history_table_header');
						$table_content = get_sub_field('history_table_content');
						if($table_header){$str .= '<th class="label">'.esc_html($table_header).'</th>';}
						if($table_content){$str .= '<td>'.wp_kses_post($table_content).'</td>';}
						$str .= '</tr>';
					} 
					$str .= '</table>';
					echo $str;
				}
			}

			/* =========
			 事業所一覧
			===== */
			elseif($post_slug == 'office'){
				if( have_rows('office') ){
					$str = '<table>';
					while( have_rows('office') ){
						the_row();
						$str .= '<tr>';
						//vars
						$office_name = get_sub_field('office_name');
						$office_detail = get_sub_field('office_detail');
						$map = get_sub_field('map'); 
						
						echo '<div class="office">';
						echo '<h2>'.esc_html($office_name).'</h2>';
						//echo '<div class="grid grid-fill">';
						//echo '<div class="grid_item grid_item-6 has-gutter">';
						echo '<div class="cf">';
						echo '<div class="fll"><p>'.wp_kses_post($office_detail).'</p></div>';
						//echo '</div>';
						//echo '<div class="grid_item grid_item-6 has-gutter"><div class="acf-map">';
						?>
						<div class="gmap"><div class="acf-map"><div class="marker" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>"><?php echo $map['address']; ?></div></div>
						<a href="http://maps.google.com/maps?q=<?php echo $map['address']; ?>" target="_blank">&rsaquo;&emsp;大きな地図でみる</a></div>
						<?php 
						//echo '</div></div>';
						//echo '</div>';
						echo '</div></div>';
					}
				}
			}

			/* =========
			 組織図・連絡先
			===== */
			elseif($post_slug == 'organization'){
				//組織図タイトル
				if( get_field('organization_title') ){
					echo '<h2>'.esc_html(get_field('organization_title')).'</h2>';
				}
				//組織図 画像
				if( get_field('organization_figure') ){
					$img = get_field('organization_figure');
					$imgurl = wp_get_attachment_image_src($img, 'full');
					echo '<div class="main_thumb"><img class="full-width" src="'.$imgurl[0].'" alt="" /></div>';
				}
				
				
				//組織図の表
				if( have_rows('organization_table') ){
					echo "\n<table class='table4'>\n<thead>\n<tr>\n<th colspan=\"2\"></th>\n<th>担当業務</th>\n</tr>\n</thead>\n";
					while( have_rows('organization_table') ){
						the_row();
						if( have_rows('unit') ){
							echo "<tbody>\n";
							while( have_rows('unit') ){
								the_row();
								
								if( have_rows('department_responsibility') ){
									//　unitの中の繰り返しフィールドの数を取得するためのループ
									$count_repeater = 0;
									while( have_rows('department_responsibility') ){
										the_row();
										$count_repeater++;
									}
									//unitの中の1行目の出力
									echo "<tr>\n";
									echo "<td rowspan=\"".$count_repeater."\">".esc_html(get_sub_field('division'))."</td>\n";
									//　echo用のループ
									$count_repeater = 0;
									while( have_rows('department_responsibility') ){
										the_row();
										$count_repeater++;
										//1行目ではない場合のみ
										if($count_repeater > 1){
											echo "<tr>\n";
										}
										//全行に共通
										echo "<td>".esc_html(get_sub_field('department'))."</td>\n";
										echo "<td>".wp_kses_post(get_sub_field('responsibility'))."</td>\n";
										echo "</tr>\n";										
									}
								}
							}
							echo "</tbody>\n";
						}					
					}
					echo '</table>';
				}
				
				
				// 連絡先 タイトル
				if( get_field('contact_title') ){
					echo '<h2>'.esc_html(get_field('contact_title')).'</h2>';
				}
				// 連絡先 内容
				if( have_rows('contact_content') ){
					while( have_rows('contact_content') ){
						the_row();
						//vars
						$office_name = get_sub_field('contact_office_name');
						$contact_address = get_sub_field('contact_address');
						//echo
						echo '<section class="office_area">';
						echo '<h2>'.esc_html($office_name).'</h2>';
						echo '<div class="cf">';
						echo '<p class="address">'.esc_html($contact_address).'</p>';
						echo '<p class="map"><a href="http://maps.google.com/maps?q='.$contact_address.'" target="_blank">MAP</a></p>';
						echo '</div>';
						echo '<table>';
						if( have_rows('contact_table')){
							while( have_rows('contact_table') ){
								the_row();
								//vars
								$table_header = get_sub_field('contact_table_header');
								$table_content = get_sub_field('contact_table_content');
								if($table_header or $table_content){
									echo '<tr>';
									echo '<th>'.esc_html($table_header).'</th>';
									echo '<td>'.wp_kses_post($table_content).'</td>';
									echo '</tr>';
								}
							}
						
						echo '</table>';
						echo '</section>';
						
					}
					}
				}
			}

			/* =========
			 有資格者数
			===== */
			elseif($post_slug == 'qualified'){
				//有資格者数 リード文
				if( get_field('qualified_lead') ){
					echo '<h2>'.esc_html(get_field('qualified_lead')).'</h2>';
				}
				//会社概要の表
				if( have_rows('qualified_table') ){
					$str = '<table class="table2">';
					if( get_field('qualified_table_caption') ){
						$str .= '<caption>';
						$str .= get_field('qualified_table_caption');
						$str .= '</caption>';
					}
					while( have_rows('qualified_table') ){
						the_row();
						$str .= '<tr>';
						//vars
						$table_header = get_sub_field('qualified_table_header');
						$table_content = get_sub_field('qualified_table_content');
						if($table_header){$str .= '<th class="label">'.esc_html($table_header).'</th>';}
						if($table_content){$str .= '<td>'.esc_html($table_content).'</td>';}
						$str .= '</tr>';
					} 
					$str .= '</table>';
					echo $str;
				}
			}

			/* =========
			 決算公告
			===== */
			elseif($post_slug == 'ir'){
				//このページに表示するパンフレット
				get_template_part('pamphlets');

			}

			/* =========
			 会社案内パンフレット
			===== */
			elseif($post_slug == 'pamphlet'){
				//このページに表示するパンフレット
				$posts = get_field('pamphlets_download');
				get_template_part('pamphlets');
			}

			/* =========
			 品質・環境・CSR
			===== */
			elseif($post_slug == 'csr'){
				if( have_rows('csr_lead') ){
					while( have_rows('csr_lead') ){
						the_row();
						//vars
						$subtitle = get_sub_field('csr_lead_title');
						$subcontent = get_sub_field('csr_lead_content');
						echo '<div class="clear">';
						if($subtitle){echo '<h2>'.esc_html($subtitle).'</h2>';}
						if($subcontent){echo '<div>'.wp_kses_post($subcontent).'</div>';}
						echo '</div>';
					} 
				}
				if( have_rows('csr_content') ){
					while( have_rows('csr_content') ){
						the_row();
						//vars
						$subtitle = get_sub_field('csr_subtitle');
						$subcontent = get_sub_field('csr_subcontent');
						echo '<div class="wysiwyg_content clear">';
						if($subtitle){echo '<h2>'.esc_html($subtitle).'</h2>';}
						if($subcontent){echo '<div>'.wp_kses_post($subcontent).'</div>';}
						echo '</div>';
					} 
				}
			
			}




		?>
	</div><!-- .entry-content -->
	<footer class="entry-footer">
		<?php twentyfifteen_entry_meta(); ?>
		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
