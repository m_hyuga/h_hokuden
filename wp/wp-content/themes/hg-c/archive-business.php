<?php
/**
 * The template for displaying "business" post type archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        <div class="mainImg_bg">
		<p>事業案内</p><div class="mainImg"><img src="<?php echo get_template_directory_uri(); ?>/common/images/common/midashi/ttl_jigyou.jpg" width="100%" alt="事業案内"></div>
        </div>
		<?php	get_template_part( 'content-pan' );?>
		<div class="content column1">
		<div class="centerCont">
		<div class="business">
			<div class="business_inner cf">
					<?php
					$businessllist = array(8, 12, 15,17,23);
					$term_cnt = 0;
					foreach ($businessllist as $business){
						$term = get_term( $business, 'business_tax' );
						$slug = $term->name; //カテゴリ名取得
						$term_cnt++;
						if ($term_cnt == 1){
							echo '<div class="fll">';
						}
						if ($term_cnt == 3){
							echo '<div class="flr">';
						}

						?>
							
						<h4 class="ternid_<?php echo $business; ?>"><?php echo $slug; ?></h4>
							<ul>
							<?php
							$args = array(
								'post_type' => 'business',
								'posts_per_page' => -1,
								'tax_query' => array(
									array(
										'taxonomy' => 'business_tax',
										'field' => 'id',
										'terms' =>  $business
									)
								)
							);
							$my_posts = get_posts( $args );
							//								dump( $my_posts );
							if ( $my_posts ) :
								foreach ( $my_posts as $post ):
									setup_postdata( $post ); ?>
									<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
								<?php endforeach;
								wp_reset_postdata();
							endif; ?>
							</ul>
								
					<?php
						if ($term_cnt == 2 or $term_cnt == 5){
							echo '</div>';
						}
					}
					?>
			</div>
			</div>
			</div>

			</div>

		</div>
		</main><!-- .site-main -->
	</section><!-- .content-area -->
<?php get_footer(); ?>
