<?php 
	if(get_field('pamphlets_download')){
		$pamphlets_posts = get_field('pamphlets_download');
	} elseif(get_sub_field('pamphlets_download')){
		$pamphlets_posts = get_sub_field('pamphlets_download');
	}
	foreach($pamphlets_posts as $p){
	//vars
	$post_id = $p->ID;
	$post_title = $p->post_title;
	$post_subtitle = get_field('subtitle', $post_id);
	$content = $p->post_content;
	$file_id = get_field('pamphlet', $post_id);
	$file_url = wp_get_attachment_url( $file_id );
	$bytes = filesize( get_attached_file( $file_id ) );
	if ($bytes >= 1073741824) $bytes = number_format($bytes / 1073741824, 2). ' GB';
    elseif ($bytes >= 1048576) $bytes = number_format($bytes / 1048576, 2). ' MB';
    elseif ($bytes >= 1024) $bytes = number_format($bytes / 1024, 2). ' KB';
    elseif ($bytes > 1) $bytes = $bytes. ' bytes';
    elseif ($bytes == 1) $bytes = $bytes. ' byte';
    else $bytes = '0 bytes';
	$file_thumbnail = wp_get_attachment_image( $file_id );
	$thumbnail_id = get_post_meta( $file_id, '_thumbnail_id', true );
	//echo
	echo '<div class="entryCont">';
	echo '<p>'. $post_title .'</p>';
	echo '<div class="cf">';
	echo '<div>';
	if($content){echo wp_kses_post($content);}
	echo '</div>';
		echo '<div class="dl_pdf">';
		if($thumbnail_id){
			$thumb_src = wp_get_attachment_image_src ( $thumbnail_id, 'medium' );
		echo '<img src="'. $thumb_src[0] .'" class="thumb">';
		}		
		echo '<a class="pdf-thumbnail-link over" href="'.$file_url.'" title="'.esc_html( $post_title ).'" target="_blank"><img src="'.get_template_directory_uri().'/common/images/common/img_dl.png" alt="ダウンロード"/><img src="'.get_template_directory_uri().'/common/images/common/pdf_sp.png" alt="PDFダウンロード" width="100%" class="spPdf"></a>';
	
	echo '<p class="size">'.$bytes.'</p>';
	echo '</div>';
	
	echo '</div>';
	echo '</div>';
}
