<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();?>
		<div class="mainImg_bg">
		<div class="mainImg">
			<img src="<?php echo get_template_directory_uri(); ?>/common/images/title/ttl_form.jpg" width="100%" alt="">
		</div>
        </div>
		<?php	get_template_part( 'content-pan' ); ?>
		
		<div class="content column1">
		<div class="entry-content">
			<h1 class="page-title"><?php the_title(); ?></h1>
			<p>業務に関するお問い合わせは、お電話または下記お問い合わせフォームから承っております。<br>お気軽にご相談下さい。</p>
			<div class="inq cf">
				<div class="tel">
					<h4>お電話でのお問い合わせはこちら</h4>
					<p>076-432-9936</p>
				</div>
				<div class="hgc">
					<h4>[HGC建築設計事務所]</h4>
					<p>0120-42-1428</p>
				</div>
			</div>
			<h3>WEBサイトからのお問い合わせはこちら</h3>
			<p class="reqtxt"><span>※</span>印部分は必須項目となります。</p>
			<div class="contact">
				<form action="">
					<div class="bgform">
						<!-- <table>
							<tr>
								<th>お名前<span>※</span></th>
								<td><input type="text" size="50" name="お名前"></td>
							</tr>
							<tr>
								<th>社名</th>
								<td><input type="text" size="50" name="社名"></td>
							</tr>
							<tr>
								<th>部門</th>
								<td><input type="text" size="50" name="部門"></td>
							</tr>
							<tr>
								<th>郵便番号</th>
								<td><input type="text" size="50" name="郵便番号" placeholder="例：930-1234"></td>
							</tr>
							<tr>
								<th>住所</th>
								<td><input type="text" size="50" name="住所"></td>
							</tr>
							<tr>
								<th>電話番号</th>
								<td><input type="text" size="50" name="電話番号" placeholder="例：000-123-4567"></td>
							</tr>
							<tr>
								<th>メールアドレス<span>※</span></th>
								<td><input type="text" size="50" name="メールアドレス"></td>
							</tr>
							<tr>
								<th>分類<span>※</span></th>
								<td>
									<label><input type="radio" name="分類" value="業務について">業務について</label>
									<label><input type="radio" name="分類" value="会社・その他について">会社・その他について</label>
								</td>
							</tr>
							<tr>
								<th>内容<span>※</span></th>
								<td><textarea name="内容" cols="30" rows="10"></textarea></td>
							</tr>
						</table> -->
						<dl class="cf">
							<dt>お名前<span>※</span></dt>
							<dd><input type="text" size="50" name="お名前"></dd>
							<dt>社名</dt>
							<dd><input type="text" size="50" name="社名"></dd>
							<dt>部門</dt>
							<dd><input type="text" size="50" name="部門"></dd>
							<dt>郵便番号</dt>
							<dd><input type="text" size="50" name="郵便番号" placeholder="例：930-1234"></dd>
							<dt>住所</dt>
							<dd><input type="text" size="50" name="住所"></dd>
							<dt>電話番号</dt>
							<dd><input type="text" size="50" name="電話番号" placeholder="例：000-123-4567"></dd>
							<dt>メールアドレス<span>※</span></dt>
							<dd><input type="text" size="50" name="メールアドレス"></dd>
							<dt class="classification">分類<span>※</span></dt>
							<dd>
								<label><input type="radio" name="分類" value="業務について">業務について</label>
								<label><input type="radio" name="分類" value="会社・その他について">会社・その他について</label>
								</dd>
							<dt>内容<span>※</span></dt>
							<dd><textarea name="内容" cols="30" rows="10"></textarea></dd>
						</dl>
					</div>
					<div class="buttonarea cf">
						<button type="reset">入力内容をリセット</button>
						<button type="submit">確認画面へすすむ</button>
					</div>
				</form>
			</div><!-- /contact -->
		</div>
		</div>
		<?php	// Include the page content template.
		// End the loop.
		endwhile;
		?>
</div>

<?php get_footer(); ?>
