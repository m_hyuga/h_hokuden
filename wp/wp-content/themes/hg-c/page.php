<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();?>
		<div class="mainImg_bg">
		<?php if(is_page('sitemap')){ ?>
			<p>サイトマップ</p><div class="mainImg"><img src="<?php echo get_template_directory_uri(); ?>/common/images/common/midashi/ttl_conpany.jpg" width="100%" alt=""></div>
		<?php }; ?>
		<?php if(is_page('contact') || is_parent_slug() === 'contact'){ ?>
			<p>お問い合わせ</p><div class="mainImg"><img src="<?php echo get_template_directory_uri(); ?>/common/images/common/midashi/ttl_form.jpg" width="100%" alt=""></div>
		<?php }; ?>
		<?php if(is_page('privacy')){ ?>
			<p>個人情報保護方針</p><div class="mainImg"><img src="<?php echo get_template_directory_uri(); ?>/common/images/common/midashi/ttl_conpany.jpg" width="100%" alt=""></div>
		<?php }; ?>
        </div>
		<?php	get_template_part( 'content-pan' ); ?>
		
		<div class="content column1">

		<div class="centerCont">
		<?php	// Include the page content template.
			get_template_part( 'content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		// End the loop.
		endwhile;
		?>
	</div><!-- /コンテンツここまで -->
</div>

<?php get_footer(); ?>
