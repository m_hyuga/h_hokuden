<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();
		
		if ((is_single('hgc-architects')) || (is_singular('specialists'))){
				get_template_part( 'content', get_post_type() );
			} else {
			?>
	<div class="mainImg_bg">
			<?php
				if (is_singular('business')){//事業案内
					echo '<p>事業案内</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/common/midashi/ttl_jigyou.jpg" width="100%" alt="事業案内"></div>';
				}
				if (is_singular('works')){//実績紹介
					echo '<p>実績紹介</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/common/midashi/ttl_jisseki.jpg" width="100%" alt="実績紹介"></div>';
				}
				if (is_single('small-hydro') || is_parent_slug() === 'small-hydro'){ //小水力発電
					echo '<p><span>わたしたちの提案</span>小水力発電</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/common/midashi/ttl_chousuiryoku.jpg" width="100%" alt="小水力発電"></div>';
				}
				if (is_single('welfare-facilities') || is_parent_slug() === 'welfare-facilities'){ //介護福祉施設
					echo '<p><span>わたしたちの提案</span>介護福祉施設</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/common/midashi/ttl_kaigo.jpg" width="100%" alt="介護福祉施設"></div>';
				}
				if (is_single('structural-design-p')){ //構造設計
					echo '<p><span>わたしたちの提案</span>構造設計</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/common/midashi/ttl_kouzou.jpg" width="100%" alt="構造設計"></div>';
				}
				if (is_singular('quality')){
					echo '<p>品質への取り組み</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/common/midashi/ttl_hinshitsu.jpg" width="100%" alt="品質への取り組み"></div>';
				}
				if (is_singular('recruit') || is_singular('senior')){
					echo '<p>採用情報</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/common/midashi/ttl_saiyou.jpg" width="100%" alt="採用情報"></div>';
				}
				if (is_singular('company')){
					echo '<p>会社案内</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/common/midashi/ttl_conpany.jpg" width="100%" alt="会社案内"></div>';
				}
				if (is_singular('post')){
					echo '<p>お知らせ</p><div class="mainImg"><img src="'.get_template_directory_uri().'/common/images/common/midashi/ttl_conpany.jpg" width="100%" alt="お知らせ"></div>';
				}
			?>
            </div>
			<?php	get_template_part( 'content-pan' );
			$post_type = get_post_type();
			if( (is_front_page()) || (is_archive() && ($post_type == 'business')) || is_single('recruit-top') || (get_field('has_sidebar') == 'サイドバー無' || is_singular('quality') || is_singular('specialists'))){
				echo '<div class="content column1">';
				if (!is_singular( 'recruit')){
					echo '<div class="centerCont"><!-- 真ん中コンテンツここから -->';
				}
				get_template_part( 'content', get_post_type() );
				if (!is_singular( 'recruit')){
					echo '</div>';
				}
			} else {
				echo '<div class="content column2">';
				get_sidebar(); 
				echo '<div class="rightCont '.attribute_escape( $post->post_name ).'"><!-- 右コンテンツここから -->';
				get_template_part( 'content', get_post_type() );
				echo '</div><!-- /右コンテンツここまで -->';
			};
		
		}
		// End the loop.
		endwhile;
		?>
	
</div>

<?php get_footer(); ?>
