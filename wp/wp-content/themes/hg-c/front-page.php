<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<script>
jQuery(window).on('load resize', function() {
	jQuery('.proposition li').tile(3);
});
</script>

<div id="primary" class="content-area">
<main id="main" class="site-main" role="main">

<div class="top_bg">
	<div class="mainImg">
	<p><span>自然と社会を<br>未来につなぐ</span></p>
	</div>
	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/common/images/top/img_main_sp.jpg" width="100%" alt="" class="img_sp">
</div>
<div class="content">
<?php
	if(get_field('show_field') == '表示する'){
		echo '<div class="notice">';
		echo '<h4><img src="'.get_template_directory_uri().'/common/images/common/icon_notice.png" width="100%" alt="">重要なお知らせ</h4>';
		echo '<div>'.get_field('txt_field').'</div>';
		echo '</div>';
	}
?>
<?php	
$args = array( 'post_type' => 'recruit', 'numberposts' => 5,'include'=>25 );
$postslist = get_posts( $args );
foreach ( $postslist as $post ) :
  setup_postdata( $post ); 
	if(get_field('show_choice') == '表示する'){
?>
		<div class="recruitBox cf">
			<div class="fll">
				<h3>採用情報<span>/</span><span class="en">recruit</span></h3>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>?recruit=recruit-top" class="over"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/common/images/common/arrow_white.png" width="100%" alt="">2016 新卒採用情報</a>
			</div>
			<div class="flr">
				<h4>採用についてのお知らせ</h4>
				<dl class="cf">
					<?php
					$args = array(
						'posts_per_page'   => 3,
						'category'         => 4
					);
					$news_posts = get_posts($args);
					if($news_posts){
							foreach($news_posts as $p){
							$str .= '<dt>';
							$str .= mysql2date( 'Y.m.d', $p->post_date );
							$str .= ' </dt>';
							$str .= '<dd><a href="'.$p->guid.'">';
							$str .= $p->post_title;
							$str .= '</a></dd>';
						} //end foreach
						echo $str;
						echo '</div>';
					} //end if
		
					?>
				</dl>
			</div>
		</div>
<?php	} else { echo '<div class="no_recruitBox"></div>';};
	endforeach; 
	wp_reset_postdata();
?>

<div class="proposition">
	<h4>わたしたちの提案<br><span>proposition</span></h4>
	<ul class="cf">
		<?php
		$proposallist = array(7, 15, 17);
		foreach ($proposallist as $proposal){
			$posts = get_posts(array("include"=>$proposal, "post_type"=>'proposal')); global $post; 
			if($posts): foreach($posts as $post): setup_postdata($post); $id = $post->ID;
				echo '<li>';
				//画像(返り値は「画像ID」)
							$attachment_id = get_field('lead_images01', $id);
							$size = "full"; // (thumbnail, medium, large, full or custom size)
							$image = wp_get_attachment_image_src( $attachment_id, $size );
							if (!empty($image)){
								echo '<a href="'.get_the_permalink().'" class="over"><img src="';
								echo $image[0];
								echo '" width="100%"></a>';
							}
				echo '<h5><a href="'.get_the_permalink().'">'.get_the_title().'</a></h5>';
				echo '<div>'.esc_html(get_field('lead_sentence', $id)).'</div>';
				echo '<p class="btn_area"><a href="'.get_the_permalink().'" class="over"><img src="'.get_template_directory_uri().'/common/images/common/arrow_gray.png" width="100%" alt="">詳細をみる</a></p>';
				echo '</li>';
			endforeach; endif; wp_reset_postdata();
			}
		 ?>
	</ul>
	<div class="teianList"><a href="<?php echo esc_url( home_url( '/' ) ); ?>?post_type=proposal" class="over"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/common/images/common/arrow_white.png" width="100%" alt="">わたしたちの提案一覧</a></div>
</div>

<?php
get_template_part('news');
?>

<h2>北電技術コンサルタントについて</h2>
<ul class="about cf">
	<?php
		$posts = get_posts(array("include"=>19, "post_type"=>'quality')); global $post; 
		if($posts): foreach($posts as $post): setup_postdata($post); $id = $post->ID;
			echo '<li class="abList1"><a href="'.get_the_permalink().'">';
			echo '<div class="img_about01_on hover_on"></div>';
			echo '<h4>'.get_the_title().'</h4>';
			echo '<p>'.get_the_excerpt().'</p>';
			echo '<div class="detaBtn"><img src="'.esc_url( get_template_directory_uri() ).'/common/images/common/arrow_white.png" width="100%" alt="">詳細をみる</div>';
			echo '</a></li>';
		endforeach; endif; wp_reset_postdata();
	 ?>
	<li class="abList2"><a href="<?php echo esc_url( home_url( '/' ) ); ?>?post_type=works">
		<div class="img_about02_on hover_on"></div>
		<h4>実績紹介<br><span>works</span></h4>
	</a></li>
	<li class="abList3"><a href="<?php echo esc_url( home_url( '/' ) ); ?>?company=company-top">
		<div class="img_about03_on hover_on"></div>
		<h4>会社案内<br><span>company</span></h4>
	</a></li>
</ul>

<div class="business">
	<h4 class="busiTitle">事業案内<span>&emsp;/&emsp;</span><span class="news">business</span></h4>
	<div class="business_inner cf">
					<?php
					$businessllist = array(8, 12, 15,17,23);
					$term_cnt = 0;
					foreach ($businessllist as $business){
						$term = get_term( $business, 'business_tax' );
						$slug = $term->name; //カテゴリ名取得
						$term_cnt++;
						if ($term_cnt == 1){
							echo '<div class="fll">';
						}
						if ($term_cnt == 3){
							echo '<div class="flr">';
						}

						?>
							
						<h4 class="ternid_<?php echo $business; ?>"><?php echo $slug; ?></h4>
							<ul>
							<?php
							$args = array(
								'post_type' => 'business',
								'posts_per_page' => -1,
								'tax_query' => array(
									array(
										'taxonomy' => 'business_tax',
										'field' => 'id',
										'terms' =>  $business
									)
								)
							);
							$my_posts = get_posts( $args );
							//								dump( $my_posts );
							if ( $my_posts ) :
								foreach ( $my_posts as $post ):
									setup_postdata( $post ); ?>
									<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
								<?php endforeach;
								wp_reset_postdata();
							endif; ?>
							</ul>
								
					<?php
						if ($term_cnt == 2 or $term_cnt == 5){
							echo '</div>';
						}
					}
					?>
	</div>
</div>

<h2 class="ttl_hgc">HGC建築設計事務所について</h2>
<div class="hgc_area cf">
	<ul class="fll cf">
	<?php
		$posts = get_posts(array("include"=>21, "post_type"=>'arch')); global $post; 
		if($posts): foreach($posts as $post): setup_postdata($post); $id = $post->ID;
			echo '<li class="abList1"><a href="'.get_the_permalink().'">';
			echo '<div class="img_hgc01_on hover_on"></div>';
			echo '<h4>'.get_the_title().'</h4>';
			echo '<p>'.esc_html(get_field('lead_sentence', $id)).'</p>';
			echo '<div class="detaBtn"><img src="'.esc_url( get_template_directory_uri() ).'/common/images/common/arrow_white.png" width="100%" alt="">詳細をみる</div>';
			echo '</a></li>';
		endforeach; endif; wp_reset_postdata();
	 ?>
		<li class="abList2"><a href="<?php echo esc_url( home_url( '/' ) ); ?>?post_type=works">
			<div class="img_hgc02_on hover_on"></div>
			<h4>実績紹介<br><span>works</span></h4>
		</a></li>
	</ul>
	<div class="blog">
		<h4>HGCトピックス</h4>
		<dl>
		<?php
		$rss = fetch_feed('http://www.hg-c.co.jp/hgc-topics/'); // RSSのURLを指定
		if (!is_wp_error( $rss ) ) :
			$maxitems = $rss->get_item_quantity(3); // 表示する記事の最大件数
			$rss_items = $rss->get_items(0, $maxitems); 
		endif;
		?>
		<?php
		if ($maxitems == 0):;
		else :
		date_default_timezone_set('Asia/Tokyo');
		foreach ( $rss_items as $item ) : ?>
			<dt><?php echo $item->get_date('Y.m.d'); ?></dt>
			<dd><a href="<?php echo $item->get_permalink(); ?>" target="_blank"><?php echo $item->get_title(); ?></a></dd>
		<?php endforeach; ?>
		<?php endif; ?>
		</dl>
		<p class="btn_area"><a href="http://www.hg-c.co.jp/hgc-topics/" target="_blank" class="over"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/common/images/common/arrow_white.png" width="100%" alt="">ブログ一覧へ</a></p>
	</div>
</div>

</div><!-- /content -->
</main><!-- .site-main -->
</div><!-- .content-area -->



<?php get_footer(); ?>
