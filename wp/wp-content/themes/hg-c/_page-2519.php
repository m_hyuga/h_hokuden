<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();?>
		<div class="mainImg_bg">
		<div class="mainImg">
		<img src="<?php echo get_template_directory_uri(); ?>/common/images/title/ttl_saitemap.jpg" width="100%" alt="">
		</div>
        </div>
		<?php	get_template_part( 'content-pan' ); ?>
		
		<div class="content column1">

		<div class="entry-content" id="sitemap"><!-- 中央コンテンツここから -->
		<?php	// Include the page content template.
			get_template_part( 'content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		// End the loop.
		endwhile;
		?>
	</div><!-- / 中央コンテンツここまで -->
</div>

<?php get_footer(); ?>
