<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、MySQL、テーブル接頭辞、秘密鍵、ABSPATH の設定を含みます。
 * より詳しい情報は {@link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 * wp-config.php の編集} を参照してください。MySQL の設定情報はホスティング先より入手できます。
 *
 * このファイルはインストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さず、このファイルを "wp-config.php" という名前でコピーして直接編集し値を
 * 入力してもかまいません。
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', '_paddlech_wizwhp');

/** MySQL データベースのユーザー名 */
define('DB_USER', '_paddlech_wizwhp');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', '5fdt45ocncg3');

/** MySQL のホスト名 */
define('DB_HOST', 'mysql507.heteml.jp');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4*jxBo*tz9=KIVWgU<hO?Wm9kKv9}aq^iQ-$`?x:?%ZbZ;|:C8cWVEV{sG`-!(Fj');
define('SECURE_AUTH_KEY',  'WQnvja92eZU_ovI-u&hGK]g^d6Ee$oEZ_-L8mL1H%Lgt|*4.:bTYy?vS<@=?8Vt^');
define('LOGGED_IN_KEY',    'h`[Jl.vpYRYzaT#o;VD%o?W5Jn;I}9f9`1K9%`pcrESH2U<)+T-yniu<L9Zo9`p_');
define('NONCE_KEY',        '<?_[P%x{JGiB4yJ$IU2;^mCjl5Nl[^9IM_d5}R[l13)vsQz[&rG}Un!{jP!4m!C?');
define('AUTH_SALT',        '`9:YRGAJAH`.2:D76o{RR}r@ss;VuSo+S3i%~#NdKmqC1:Ay2bFK4<fm9u}tC)3y');
define('SECURE_AUTH_SALT', '9b2AdG8*.oK%2p2vG|`AO4)OpF}-M[9Nw3rQ~pZLU$PNtI~?kYU?Tch~!9KO@L6)');
define('LOGGED_IN_SALT',   'EnYhT>/)[dU&&x`pMt89Mg~Ngo61?28XgX`?pI{!&v)-jC0<<z=cWQ,]`(/P9.(f');
define('NONCE_SALT',       'kCzQo<Su#Z5l5w2-l+n}axUz4hE4uLaUrP%WgWGi;BK-=C^3L![(zq6uOBo}rFxZ');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

add_filter('xmlrpc_enabled', '__return_false');

add_filter('xmlrpc_methods', function($methods) {
    unset($methods['pingback.ping']);
    unset($methods['pingback.extensions.getPingbacks']);
    return $methods;
});