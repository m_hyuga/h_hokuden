<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', '_paddlech_qfajgo');

/** MySQL データベースのユーザー名 */
define('DB_USER', '_paddlech_qfajgo');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'ouzagjdwmf3gw');

/** MySQL のホスト名 */
define('DB_HOST', 'mysql502.heteml.jp');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0caEIuM}5v9N{;9TI8#y-`11R,^.7C<x9q[HbikXUk%uz%E}$G<;f*)=>AA<hQ$g');
define('SECURE_AUTH_KEY',  '|`*28sPTSTEIYmkc/1rt9y9>>BE0vIW/hf&gNU?9*YJhA/~N&,{&8|^=cb)O$ebl');
define('LOGGED_IN_KEY',    '~hHr]brB@@#UEC#JM~OLamc^DkmGEE.h&UP|<{cGRf50!7AN{t31VV:3{0Ae`MCA');
define('NONCE_KEY',        '9vb=._Xe3SV4PzIqjCh_2w=>nRx#8?D9&8<>^nx<`I-p}UW!0_{s,O!y9y18*_7/');
define('AUTH_SALT',        '^/CQ0?h<ct64DT6_r])2k*rk6ii%6f>@tUkkoH1G@yBYH|w?U4-_#398bh#YD[s=');
define('SECURE_AUTH_SALT', 'pT1T5t4<h`o+Xb}-VKGYDGW9*ZYMuHda4V:zE*&(Eu;6MN*!2Oa`y<`6qxIa`([?');
define('LOGGED_IN_SALT',   'bF+9N:(w9[sLj0NbF)1__<[%%efAl0TC`cCnrJZqeI]D~j9ir07&)?+.yoMZe954');
define('NONCE_SALT',       'd}hKm|[GjOo#=nl8ej$l2/xq2AGWACR7f>IHPer?.W[A~7}7gCjZQXGI1q9].Q?r');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

add_filter('xmlrpc_enabled', '__return_false');

add_filter('xmlrpc_methods', function($methods) {
    unset($methods['pingback.ping']);
    unset($methods['pingback.extensions.getPingbacks']);
    return $methods;
});