<?php
/**
 * The sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
		<nav class="widget">
			<div class="hentry">
				<ul class="sideMenu">
				<li class="linkTop cat-item-compensation"><a href="http://www.hg-c.co.jp/hgc-topics/"><span>トップページ</span></a></li>
				<li class="cat-item-compensation"><span>カテゴリー</span>
					<ul class="children">
					<?php 
					$slug = $cat;
					$cat_all = get_terms( "category", "fields=all&get=all" );
					foreach($cat_all as $value):
					echo '<li';
					$listslug = $value->term_id;
					$listslug = intval($listslug);
					if ($listslug === $slug ){ echo ' class="current_page_item"';}
					echo '><a href="'.get_category_link($value->term_id).'">'.$value->name.'</a></li>';
					endforeach; ?>
					</ul>
				</li>
				<li class="cat-item-compensation"><span>アーカイブ</span>
					<ul class="children">
					<?php wp_get_archives('show_post_count=true&type=monthly'); ?>
					</ul>
				</li>
				<li class="backTop cat-item-compensation"><a href="http://www.hg-c.co.jp/"><span>本サイトへ戻る</span></a></li>
				</ul>
			</div>
		</nav>
