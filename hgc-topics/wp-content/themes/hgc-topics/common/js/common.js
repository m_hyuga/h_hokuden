jQuery(function(){

//発火
pullDown();
pageTop();
spMenuSub();
spMenuBtn();
sideNav();
recruitApp();
hoverOver();
if(jQuery('#top').size()) {
	//TOPのみの処理
	topAbout();
} else {
	//TOP以外のみの処理
	acMenu();
}

//アコーディオンメニュ
function acMenu() {
	jQuery('#acMenu dt').on('click', function(){
		jQuery(this).next().slideToggle();
		jQuery(this).toggleClass('onclick');
		if(jQuery(this).hasClass('onclick')) {
			jQuery(this).children('.arrow').children('img').attr('src', 'common/images/common/btn_top.png');
		} else {
			jQuery(this).children('.arrow').children('img').attr('src', 'common/images/common/btn_bottom.png');
		}
	});
}

//PCメニューのオーバーダウンメニュ
// function pullDown() {
// 	jQuery('.pulldownMenu').on('click', function(){
// 		jQuery(this).toggleClass('onPull');
// 		if(jQuery(this).hasClass('onPull')) {
// 			jQuery('.innerMenu').stop(true,false).fadeIn();
// 		} else {
// 			jQuery('.innerMenu').stop(true,false).fadeOut();
// 		}
// 	});
// }
function pullDown() {
	jQuery('.pulldownMenu').hover(function(){
		jQuery('.innerMenu').stop(true,false).fadeIn();
	}, function(){
		jQuery('.innerMenu').stop(true,false).fadeOut();
	});
}

function spMenuBtn() {
	jQuery(window).on('load resize', function(){
		var btnWidth = jQuery('#spBtn').width();
		jQuery('#spBtn').height(btnWidth);
	});
	jQuery('#spBtn').on('click', function(){
		jQuery(this).toggleClass('open');//押されたらクラス持たせる
		if(jQuery(this).hasClass('open')) {
			jQuery('#spMenu').stop(true,false).fadeIn();
		} else {
			jQuery('#spMenu').stop(true,false).fadeOut();
		}
	});
}
function spMenuSub() {
	jQuery('.spInnerBtn').on('click', function(){
		jQuery(this).toggleClass('subOp');//押されたらクラス持たせる
		if(jQuery(this).hasClass('subOp')) {
			jQuery('#subInnerMenu').stop(true,false).fadeIn();
		} else {
			jQuery('#subInnerMenu').stop(true,false).fadeOut();
		}
	});
}

function pageTop() {
	jQuery('#pagetop').on('click', function(){
		jQuery('body, html').animate({ scrollTop: 0 }, 500);
	});
}

function sideNav() {
	jQuery(window).on('load resize', function(){
		if(jQuery(window).width() <= 768) {
			jQuery('.sideMenu').appendTo('#spMenu');
		} else {
			jQuery('.sideMenu').prependTo('.column2');
			jQuery('#spMenu').css({'display':'none'});
			jQuery('#spBtn.open').removeClass('open');
		}
	});
}

function topAbout() {
	jQuery('.about li a,.hgc_area ul li a').hover(function(){
		jQuery(this).children('img').stop(true,false).fadeIn(700);
	}, function(){
		jQuery(this).children('img').stop(true,false).fadeOut(400);
	});
}

function recruitApp() {
	jQuery('.appArea li a').hover(function(){
		jQuery(this).children('.onbg').stop(true,false).fadeIn(700);
	}, function(){
		jQuery(this).children('.onbg').stop(true,false).fadeOut(400);
	});
}



function hoverOver() {
	jQuery(".over") .hover(function(){
			 jQuery(this).stop().animate({'opacity' : '0.6'}, 180); // マウスオーバーで透明度を30%にする
		},function(){
			 jQuery(this).stop().animate({'opacity' : '1'}, 100); // マウスアウトで透明度を100%に戻す
	});
}

});