<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/common/images/common/favicon.ico" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="header" class="cf">
	<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php bloginfo('template_url'); ?>/common/images/common/img_h1.jpg" width="100%" alt="北電技術コンサルタント株式会社" class="over"></a></h1>
	<!-- SPメニュ ここから -->
	<div id="spBtn"></div>
	<div id="spMenu">
	</div>
	<!-- /SPメニュ ここまで -->
	<div id="nav">
		<div class="nav_top cf">
			<div class="logo"><img src="<?php bloginfo('template_url'); ?>/common/images/common/img_logo.jpg" width="100%" alt="北陸電力グループ"></div>
		</div>
	</div><!-- /nav -->
</div><!-- /header -->

<div class="mainImg">
	<img src="<?php bloginfo('template_url'); ?>/common/images/blog/img_main.jpg" width="100%" alt="">
	<a href="http://hg-c.paddlechart.com/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/arrow_white.png" width="100%" alt="">本サイトへ戻る</a>
</div>
<div class="content column2">

<?php get_sidebar(); ?>
