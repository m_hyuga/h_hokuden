<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
		<?php
		if ( is_single() ) : ?>
		<div class="article_deta">
			<div class="cf">
				<h2><?php the_title(); ?></h2>
				<p class="date"><?php the_time('Y.m.d'); ?></p>
				<div class="cate"><?php the_category(); ?></div>
			</div>
			<?php
					if (has_post_thumbnail()){
						echo '<img src="';
						$image_id = get_post_thumbnail_id();
						$image_url = wp_get_attachment_image_src($image_id, true);
						echo  $image_url[0];
						echo '" alt="" class="thumb">';
					}
				?>
			<div class="detatxt">
			<?php the_content(); ?>
			</div>
		</div>
		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>


	<?php	else : ?>
		<div class="article">
			<div class="cf">
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<p class="date"><?php the_time('Y.m.d'); ?></p>
				<div class="cate"><?php the_category(); ?></div>
			</div>
			<div class="cf">
				<?php
					if (has_post_thumbnail()){
						echo '<img src="';
						$image_id = get_post_thumbnail_id();
						$image_url = wp_get_attachment_image_src($image_id, true);
						echo  $image_url[0];
						echo '" width="100%" alt="">';
					}
				?>
				<div class="blogtxt">
					<p class="detatxt"><?php
					$theContentForPreSingle = mb_substr(strip_tags($post-> post_content), 0, 200);
					$theContentForPreSingle = preg_replace('/\［caption(.+?)\/caption\］/','',$theContentForPreSingle);
					$theContentForPreSingle = preg_replace('/\［gallery(.+?)\］/','',$theContentForPreSingle);
					echo $theContentForPreSingle;
					if(mb_strlen($theContentForPreSingle) >= 200){echo '...';};
					?></p>
					<a href="<?php the_permalink(); ?>" class="over">続きを見る</a>
				</div>
			</div>
		</div>
	<?php	endif;	?>

</article><!-- #post-## -->
