<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div class="rightCont"><!-- 右コンテンツここから -->
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<div class="article_deta">
			<div class="cf">
				<h4>ページが見つかりませんでした</h4>
			</div>
			<div class="detatxt">申し訳ありません。お探しのページは見つかりませんでした。</div>
		</div>

		</main><!-- .site-main -->
	</div><!-- .content-area -->
	</div>
</div>

<?php get_footer(); ?>
