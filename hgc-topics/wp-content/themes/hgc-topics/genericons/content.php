<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		if ( is_single() ) : ?>
		
		<header class="entry-header">
			<h1></h1>
		</header><!-- .entry-header -->
			<div class="entry-content">
				<?php
					/* translators: %s: Name of current post */
					the_content( sprintf(
						__( 'Continue reading %s', 'twentyfifteen' ),
						the_title( '<span class="screen-reader-text">', '</span>', false )
					) );
		
					wp_link_pages( array(
						'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfifteen' ) . '</span>',
						'after'       => '</div>',
						'link_before' => '<span>',
						'link_after'  => '</span>',
						'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>%',
						'separator'   => '<span class="screen-reader-text">, </span>',
					) );
				?>
			</div><!-- .entry-content -->
		
			<?php
				// Author bio.
				if ( is_single() && get_the_author_meta( 'description' ) ) :
					get_template_part( 'author-bio' );
				endif;
			?>
		
			<footer class="entry-footer">
				<?php twentyfifteen_entry_meta(); ?>
				<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
			</footer><!-- .entry-footer -->


	<?php	else : ?>
		<div class="article">
			<div class="cf">
				<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
				<p class="date"><?php the_time('Y.m.d'); ?></p>
			</div>
			<div class="cate"><?php the_category(); ?></div>
			<div class="cf">
				<?php
					if (has_post_thumbnail()){
						echo '<img src="';
						$image_id = get_post_thumbnail_id();
						$image_url = wp_get_attachment_image_src($image_id, true);
						echo  $image_url[0];
						echo '" width="100%" alt="">';
					}
				?>
				<div class="blogtxt">
					<p><?php
					$theContentForPreSingle = mb_substr(strip_tags($post-> post_content), 0, 200);
					$theContentForPreSingle = preg_replace('/\［caption(.+?)\/caption\］/','',$theContentForPreSingle);
					$theContentForPreSingle = preg_replace('/\［gallery(.+?)\］/','',$theContentForPreSingle);
					echo $theContentForPreSingle;
					if(mb_strlen($theContentForPreSingle) >= 200){echo '...';};
					?></p>
					<a href="<?php the_permalink(); ?>" class="over">続きを見る</a>
				</div>
			</div>
		</div>
	<?php	endif;	?>
</header><!-- .entry-header -->

</article><!-- #post-## -->
